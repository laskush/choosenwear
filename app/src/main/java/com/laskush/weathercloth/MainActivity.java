package com.laskush.weathercloth;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.laskush.weathercloth.Cloth.Activity.BestMatchActivity;
import com.laskush.weathercloth.Cloth.Activity.ClothUploadActivity;
import com.laskush.weathercloth.Cloth.Activity.GenderSelectionActivty;
import com.laskush.weathercloth.Cloth.Adapter.ReminderItemAdapter;
import com.laskush.weathercloth.Cloth.Model.ImageDTO;
import com.laskush.weathercloth.Cloth.Model.ReminderItem;
import com.laskush.weathercloth.Cloth.fragment.SliderFragment;
import com.laskush.weathercloth.Remainder.RemainderActivity;
import com.laskush.weathercloth.Reminder1.ReminderActivity1;
import com.laskush.weathercloth.Utils.AppUtil;
import com.laskush.weathercloth.Utils.PrefUtils;
import com.laskush.weathercloth.Weather.activity.WeatherActivity;
import com.laskush.weathercloth.Weather.activity.WeatherListActvitiy;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

//implements BaseSliderView.OnSliderClickListener,ViewPagerEx.OnPageChangeListener
public class MainActivity extends AppCompatActivity {

    //constant
    private static String TAG= MainActivity.class.getSimpleName();

    //Views
    CardView bestmatch_cardView,uploadcloth_cardView,weather_cardView;
    //CardView umbrella_cardView,shoe_cardView,wallet_cardView,watch_cardView;
    TextView reminder_tv;
   /* private SliderLayout view_image_slider;
    private PagerIndicator custom_image_indicator;*/
    RelativeLayout main_no_cloths_rl;
    LinearLayout upload_ll;
    ViewPager viewPagerImages;
    FloatingActionButton reminderitem_fab;
    RecyclerView reminderitem_recyclerView;
    public String folderName = "Choose n Wear";



    TextView toolbar_tv;
    Toolbar toolbar;

    //Instances
    RequestQueue requestQueue;
    ProgressDialog progressDialog;
    ReminderItemAdapter adapter;

    //variables
    ArrayList<ImageDTO> sliderArrayList = new ArrayList<>();
    static String setDirectoryName;
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    public static final int PERMISSIONS_MULTIPLE_REQUEST = 123;
    static String genderValue;
    int currentSliderPage = 0;
    int totalSliderCount;
    ArrayList<ReminderItem> reminderItemList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestRuntimePermission();
        }

        reminderitem_fab = (FloatingActionButton) findViewById(R.id.reminderitem_fab);
        reminderitem_recyclerView = (RecyclerView) findViewById(R.id.reminderitem_recyclerView);


        requestQueue = Volley.newRequestQueue(MainActivity.this);

        toolbar_tv = (TextView) findViewById(R.id.toolbar_tv);
        toolbar_tv.setText(getResources().getString(R.string.app_name));
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        bestmatch_cardView = (CardView) findViewById(R.id.bestmatch_cardView);
        uploadcloth_cardView = (CardView) findViewById(R.id.uploadcloth_cardView);
        weather_cardView = (CardView) findViewById(R.id.weather_cardView);

        reminder_tv = (TextView) findViewById(R.id.reminder_tv);
        /*umbrella_cardView = (CardView) findViewById(R.id.umbrella_cardView);
        shoe_cardView = (CardView) findViewById(R.id.shoe_cardView);
        wallet_cardView = (CardView) findViewById(R.id.wallet_cardView);
        watch_cardView = (CardView) findViewById(R.id.watch_cardView);*/
        /*view_image_slider = (SliderLayout) findViewById(R.id.view_image_slider);
        custom_image_indicator = (PagerIndicator) findViewById(R.id.custom_image_indicator);*/

        viewPagerImages = (ViewPager) findViewById(R.id.viewPagerImages);
        main_no_cloths_rl = (RelativeLayout) findViewById(R.id.main_no_cloths_rl);
        upload_ll = (LinearLayout) findViewById(R.id.upload_ll);


        if(PrefUtils.returnFirstRun(MainActivity.this).equalsIgnoreCase("true")){
            PrefUtils.save_firstRun(MainActivity.this,"false");

            //for reminder item
            reminderItemList.add(new ReminderItem("Umbrella"));
            reminderItemList.add(new ReminderItem("shoe"));
            reminderItemList.add(new ReminderItem("wallet"));
            reminderItemList.add(new ReminderItem("watch"));

            PrefUtils.saveReminderList(MainActivity.this,reminderItemList,PrefUtils.REMAINDER_ITEM_LIST);
            setReminderItem(reminderItemList);

            upload_ll.setVisibility(View.VISIBLE);

           /* //For reading gender value
           if(PrefUtils.isGenderSave(MainActivity.this)){

                if(PrefUtils.returnGender(MainActivity.this).equalsIgnoreCase("MALE")){
                    genderValue = "MALE";
                }else if(PrefUtils.returnGender(MainActivity.this).equalsIgnoreCase("FEMALE")){
                    genderValue = "FEMALE";
                }

               //fetching images
               getUpperClothes();
               getLowerClothes();

               if((sliderArrayList.size() > 0)){
                   Toast.makeText(MainActivity.this, "Slider List : "+sliderArrayList.size(), Toast.LENGTH_SHORT).show();
                   createSlider(sliderArrayList);
               }else{
                   upload_ll.setVisibility(View.VISIBLE);
               }

            }else{
               showGenderChangeDialog();
               upload_ll.setVisibility(View.VISIBLE);
           }*/

        }else{

            reminderItemList = PrefUtils.getReminderList(MainActivity.this,PrefUtils.REMAINDER_ITEM_LIST);
            setReminderItem(reminderItemList);

            upload_ll.setVisibility(View.GONE);
            main_no_cloths_rl.setVisibility(View.GONE);

            //For reading gender value
            if(PrefUtils.isGenderSave(MainActivity.this)){

                if(PrefUtils.returnGender(MainActivity.this).equalsIgnoreCase("MALE")){
                    genderValue = "MALE";
                }else if(PrefUtils.returnGender(MainActivity.this).equalsIgnoreCase("FEMALE")){
                    genderValue = "FEMALE";
                }

                //fetching images
                getUpperClothes();
                getLowerClothes();

                if((sliderArrayList.size() > 0)){
                    //Toast.makeText(MainActivity.this, "Slider List : "+sliderArrayList.size(), Toast.LENGTH_SHORT).show();
                    createSlider(sliderArrayList);
                }else{
                    upload_ll.setVisibility(View.VISIBLE);
                }

            }else{
                showGenderChangeDialog();
                upload_ll.setVisibility(View.VISIBLE);
            }


        }



        upload_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //For reading gender value
                if(PrefUtils.isGenderSave(MainActivity.this)){

                    if(PrefUtils.returnGender(MainActivity.this).equalsIgnoreCase("MALE")){
                        genderValue = "MALE";
                    }else if(PrefUtils.returnGender(MainActivity.this).equalsIgnoreCase("FEMALE")){
                        genderValue = "FEMALE";
                    }

                    //fetching images
                    getUpperClothes();
                    getLowerClothes();

                    if((sliderArrayList.size() > 0)){
                        //Toast.makeText(MainActivity.this, "Slider List : "+sliderArrayList.size(), Toast.LENGTH_SHORT).show();
                        upload_ll.setVisibility(View.GONE);
                        main_no_cloths_rl.setVisibility(View.GONE);
                        createSlider(sliderArrayList);
                    }else{
                        Intent i = new Intent(MainActivity.this,BestMatchActivity.class);
                        startActivity(i);
                    }

                }else{
                    showGenderChangeDialog();
                    upload_ll.setVisibility(View.VISIBLE);
                }


            }
        });
        bestmatch_cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(PrefUtils.isGenderSave(MainActivity.this)){
                    Intent bestIntent = new Intent(MainActivity.this, BestMatchActivity.class);
                    bestIntent.putExtra("ClassValue","bestselection");
                    startActivity(bestIntent);

                }else{
                    Intent bestIntent = new Intent(MainActivity.this, GenderSelectionActivty.class);
                    bestIntent.putExtra("ClassValue","bestselection");
                    startActivity(bestIntent);
                }


            }
        });

        uploadcloth_cardView.setOnClickListener(new CardView.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(PrefUtils.isGenderSave(MainActivity.this)){
                    Intent clothIntent = new Intent(MainActivity.this, ClothUploadActivity.class);
                    clothIntent.putExtra("ClassValue","allselection");
                    startActivity(clothIntent);

                }else{
                    Intent weatherIntent = new Intent(MainActivity.this, GenderSelectionActivty.class);
                    weatherIntent.putExtra("ClassValue","allselection");
                    startActivity(weatherIntent);
                }

            }
        });

        weather_cardView.setOnClickListener(new CardView.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(PrefUtils.isWeatherSave(MainActivity.this)){
                    if(AppUtil.isInternetConnectionAvailable(MainActivity.this)){
                        progressDialog = new ProgressDialog(MainActivity.this);
                        progressDialog.setMessage("Loading.......");
                        progressDialog.setTitle("Searching for Weather Info");
                        progressDialog.show();
                        HttpMethod(PrefUtils.returnCity(MainActivity.this));
                    }else{
                        showSnackbar(getString(R.string.no_internet));
                    }
                }else{
                    Intent weatherIntent = new Intent(MainActivity.this, WeatherActivity.class);
                    startActivity(weatherIntent);
                }

            }
        });

        reminder_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent reminderIntent = new Intent(MainActivity.this, ReminderActivity1.class);
                startActivity(reminderIntent);
            }
        });

       /* umbrella_cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent reminderIntent = new Intent(MainActivity.this, RemainderActivity.class);
                startActivity(reminderIntent);
            }
        });

        shoe_cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent reminderIntent = new Intent(MainActivity.this, RemainderActivity.class);
                startActivity(reminderIntent);
            }
        });

        wallet_cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent reminderIntent = new Intent(MainActivity.this, RemainderActivity.class);
                startActivity(reminderIntent);
            }
        });

        watch_cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent reminderIntent = new Intent(MainActivity.this, RemainderActivity.class);
                startActivity(reminderIntent);
            }
        });*/


        reminderitem_fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showReminderDialog();
            }
        });
    }

    //fetching weather response
    private void HttpMethod(String city_name) {

        StringRequest sr = new StringRequest(Request.Method.GET, "http://api.worldweatheronline.com/premium/v1/weather.ashx?key=49fa51f993fc4e3fbcb22603182610&q="+city_name + "&format=json&num_of_days=7",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("HttpClient", "success! response: " + response.toString());

                        if(response.toString() != null){

                            try {
                                JSONObject jsonObject = new JSONObject(response.toString());
                                if(jsonObject.has("data")){
                                    JSONObject jsonObjectData = jsonObject.getJSONObject("data");

                                    if(jsonObjectData.has("error")){

                                        JSONArray jsonArrayError = jsonObjectData.getJSONArray("error");

                                        for(int i=0; i<jsonArrayError.length();i++){
                                            JSONObject jsonObject1 = jsonArrayError.getJSONObject(0);

                                            if(jsonObject1.has("msg")){
                                                progressDialog.dismiss();
                                                String message = jsonObject1.getString("msg");
                                                showSnackbar(message);
                                            }
                                        }
                                    }else if(jsonObjectData.has("request")){

                                        progressDialog.dismiss();
                                        Intent i = new Intent(MainActivity.this,WeatherListActvitiy.class);
                                        i.putExtra("response",response.toString());
                                        startActivity(i);
                                    }


                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.e("HttpClient", "error: " + error.toString());
                        //Toast.makeText(WeatherActivity.this, "RESPONSE : NULL", Toast.LENGTH_SHORT).show();
                        showSnackbar("City not found");
                    }
                })
        {
/*            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("key","14085375aceb4090b9f113838182407");
                params.put("q","Kathmandu");
                params.put("format","json");
                params.put("num_of_days","1");
                return params;
            }*/
           /* @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }*/
        };
        requestQueue.add(sr);
    }

    void showSnackbar(String message){

        Snackbar.make(findViewById(R.id.uploadcloth_cardView), message,
                Snackbar.LENGTH_LONG).show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);

        switch(item.getItemId()){
            case R.id.weather_refresh:
                //Toast.makeText(getBaseContext(), "You selected weather", Toast.LENGTH_SHORT).show();
                showCityChangeDialog();
                break;

            case R.id.gender_refresh:
                //Toast.makeText(getBaseContext(), "You selected gender", Toast.LENGTH_SHORT).show();
                showGenderChangeDialog();
                break;

        }
        return true;

    }


    private void showReminderDialog() {

        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.add_reminder_item_dialog);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();


        final EditText reminderItem = (EditText) dialog.findViewById(R.id.reminder_item);
        ImageView close = (ImageView) dialog.findViewById(R.id.close);
        TextView add = (TextView) dialog.findViewById(R.id.add);


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(reminderItem.getText().toString().isEmpty()){
                    Toast.makeText(MainActivity.this, "enter the item name or close dialog", Toast.LENGTH_SHORT).show();
                }else{
                    reminderItemList = PrefUtils.getReminderList(MainActivity.this,PrefUtils.REMAINDER_ITEM_LIST);
                    reminderItemList.add(new ReminderItem(reminderItem.getText().toString()));
                    PrefUtils.saveReminderList(MainActivity.this,reminderItemList,PrefUtils.REMAINDER_ITEM_LIST);
                    setReminderItem(reminderItemList);
                    dialog.dismiss();
                }


            }
        });

    }


    private void showCityChangeDialog() {

        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.city_change_dialogbox);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();


        final EditText city = (EditText) dialog.findViewById(R.id.city);
        ImageView close = (ImageView) dialog.findViewById(R.id.close);
        TextView save = (TextView) dialog.findViewById(R.id.save);


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(city.getText().toString().isEmpty()){
                    city.setError("Required");
                }else{
                    PrefUtils.save_weatherValue(MainActivity.this, city.getText().toString(),true);

                    showSnackbar("City name changed");

                }
                dialog.dismiss();

            }
        });

    }


    private void showGenderChangeDialog() {

        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.gender_change_dialogbox);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

        final AppCompatCheckBox male_checkbox = (AppCompatCheckBox) dialog.findViewById(R.id.male_checkbox);
        final AppCompatCheckBox  female_checkbox = (AppCompatCheckBox) dialog.findViewById(R.id.female_checkbox);
        ImageView close = (ImageView) dialog.findViewById(R.id.close);
        TextView save = (TextView) dialog.findViewById(R.id.save);

        final String savedGender = PrefUtils.returnGender(MainActivity.this);
        final String[] newGender = new String[1];
        if(savedGender.equalsIgnoreCase("MALE")){
            male_checkbox.setChecked(true);
            female_checkbox.setChecked(false);
            newGender[0] = "MALE";
        }else if(savedGender.equalsIgnoreCase("FEMALE")){
            male_checkbox.setChecked(false);
            female_checkbox.setChecked(true);
            newGender[0] = "FEMALE";
        }


        male_checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                male_checkbox.setChecked(true);
                female_checkbox.setChecked(false);
                newGender[0] ="MALE";
            }
        });

        female_checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                male_checkbox.setChecked(false);
                female_checkbox.setChecked(true);
                newGender[0] ="FEMALE";
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(male_checkbox.isChecked() == false && female_checkbox.isChecked() == false){
                    showSnackbar("Select the gender");
                }
                if(newGender[0].equalsIgnoreCase(savedGender)){
                    if(newGender[0].equalsIgnoreCase("MALE")){
                        PrefUtils.save_genderValue(MainActivity.this, "MALE",true);
                        showSnackbar("Gender changed to male");
                        dialog.dismiss();
                    }else if(newGender[0].equalsIgnoreCase("FEMALE")){
                        PrefUtils.save_genderValue(MainActivity.this, "MALE",true);
                        showSnackbar("Gender changed to female");
                        dialog.dismiss();
                    }

                }else if(male_checkbox.isChecked() == true){

                    PrefUtils.save_genderValue(MainActivity.this, "MALE",true);
                    showSnackbar("Gender changed to male");
                    dialog.dismiss();

                }else if(female_checkbox.isChecked() == true){
                    PrefUtils.save_genderValue(MainActivity.this, "FEMALE",true);
                    showSnackbar("Gender changed to female");
                    dialog.dismiss();

                }
            }
        });

    }


    //For run time permission
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void requestRuntimePermission() {
        if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.READ_EXTERNAL_STORAGE) +
                ContextCompat.checkSelfPermission(MainActivity.this,
                        Manifest.permission.CAMERA) +
                ContextCompat.checkSelfPermission(MainActivity.this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale
                    (MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                    ActivityCompat.shouldShowRequestPermissionRationale
                            (MainActivity.this, Manifest.permission.CAMERA) ||
                    ActivityCompat.shouldShowRequestPermissionRationale
                            (MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                Snackbar.make(MainActivity.this.findViewById(android.R.id.content),
                        "Please Grant Permissions to capture image",
                        Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
                        new View.OnClickListener() {
                            @RequiresApi(api = Build.VERSION_CODES.M)
                            @Override
                            public void onClick(View v) {
                                requestPermissions(
                                        new String[]{Manifest.permission
                                                .READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                        PERMISSIONS_MULTIPLE_REQUEST);
                            }
                        }).show();
            } else {
                requestPermissions(
                        new String[]{Manifest.permission
                                .READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        PERMISSIONS_MULTIPLE_REQUEST);
            }
        } else {
            // write your logic code if permission already granted
        }

    }


    private void getUpperClothes() {

        sliderArrayList= new ArrayList<>();
        setDirectoryName = "UpperCloths";
        File root = new File(Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES+"/"+ folderName +"/"+ genderValue + "/Best Match/"),
                setDirectoryName);
        //Log.d("Root : ", String.valueOf(root));

        // Create the storage directory if it does not exist
        if (!root.exists()) {
            main_no_cloths_rl.setVisibility(View.VISIBLE);
            upload_ll.setVisibility(View.VISIBLE);
            //Toast.makeText(this, "Please Upload the Upper Cloth", Toast.LENGTH_SHORT).show();
            //showSnackbar("Please Upload the Upper Cloth");
        }else{

            ArrayList<File> fileList = new ArrayList<File>();
            fileList = getfile(root);

            for(int i = 0; i<fileList.size();i++){

                main_no_cloths_rl.setVisibility(View.GONE);
                upload_ll.setVisibility(View.GONE);

               /* Log.d("File : ", String.valueOf(fileList.indexOf(i)));
                Toast.makeText(this, "File : "+ fileList.get(i).getAbsolutePath(), Toast.LENGTH_SHORT).show();*/
                sliderArrayList.add(new ImageDTO(" ",fileList.get(i).getAbsolutePath()));
            }

        }
    }



    private void getLowerClothes() {

        setDirectoryName = "LowerCloths";
        File root = new File(Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES+"/"+ folderName +"/"+ genderValue + "/Best Match/"),
                setDirectoryName);
        //Log.d("Root : ", String.valueOf(root));

        // Create the storage directory if it does not exist
        if (!root.exists()) {
            main_no_cloths_rl.setVisibility(View.VISIBLE);
            upload_ll.setVisibility(View.VISIBLE);
            //Toast.makeText(this, "Please Upload the Lower Coths", Toast.LENGTH_SHORT).show();
            //showSnackbar("Please Upload the Lower Coths");
        }else{

            ArrayList<File> fileList = new ArrayList<File>();
            fileList = getfile(root);

            for(int i = 0; i<fileList.size();i++){
                sliderArrayList.add(new ImageDTO(" ",fileList.get(i).getAbsolutePath()));

                main_no_cloths_rl.setVisibility(View.GONE);
                upload_ll.setVisibility(View.GONE);
            }

        }
    }

    public ArrayList<File> getfile(File dir) {
        ArrayList<File> fileList = new ArrayList<File>();
        File listFile[] = dir.listFiles();

        // Toast.makeText(this, "listFile len : "+listFile.length, Toast.LENGTH_SHORT).show();
        if (listFile != null && listFile.length > 0) {
            for (int i = 0; i < listFile.length; i++) {

                if (listFile[i].getName().endsWith(".png")
                        || listFile[i].getName().endsWith(".jpg")
                        || listFile[i].getName().endsWith(".jpeg")
                        || listFile[i].getName().endsWith(".gif"))


                {
                    fileList.add(listFile[i]);
                }
            }
        }

        return fileList;
    }




    private void createSlider(List<ImageDTO> sliderList) {
        totalSliderCount = sliderList.size();
        //  AppLog.showLog(TAG, "sliderItemDTOListSize::::" + sliderItemDTOList.size());
        if (sliderList != null && sliderList.size() > 0) {

            viewPagerImages.setAdapter(new SlidePagerAdapter(getSupportFragmentManager(), sliderList));
            //  circlePageIndicator.setViewPager(viewPager);
        }


        startSliderFling();
    }
    private void startSliderFling() {

        final Handler handler = new Handler();

        final Runnable Update = new Runnable() {
            public void run() {
                if (currentSliderPage == totalSliderCount) {
                    currentSliderPage = 0;
                }
                viewPagerImages.setCurrentItem(currentSliderPage, true);
                currentSliderPage++;
            }
        };

        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {

            @Override
            public void run() {
                handler.post(Update);
            }
        }, 1000, 3000);

    }


    private class SlidePagerAdapter extends FragmentStatePagerAdapter {
        private List<ImageDTO> sliderList;

        public SlidePagerAdapter(FragmentManager fm, List<ImageDTO> mSliderList) {
            super(fm);
            this.sliderList = mSliderList;
            Log.d(TAG, "sliderListSize::" + sliderList.size());
        }

        @Override
        public Fragment getItem(int position) {
            Log.d(TAG, "getItem::pos:" + position);
            if (sliderList != null) {
                return new SliderFragment(position, sliderList.get(position));
            } else {
                return new SliderFragment(position, null);
            }
        }

        @Override
        public int getCount() {
            return sliderList != null ? sliderList.size() : 1;
        }
    }


    public  void  setReminderItem(ArrayList<ReminderItem> reminderItemList) {

        adapter=new ReminderItemAdapter(MainActivity.this,reminderItemList);
        reminderitem_recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
       /* RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(MainActivity.this, LinearLayout.HORIZONTAL,false);
        reminderitem_recyclerView.setLayoutManager(mLayoutManager);*/
        reminderitem_recyclerView.setAdapter(adapter);

    }


   /* *//* method to create the slider*//*
    public void createSlider(List<ImageDTO> sliderList) {
        for(int i=0;i<sliderList.size();i++){
            TextSliderView textSliderView = new TextSliderView(MainActivity.this);

            textSliderView
                    .description("Best Matches")
                    .image("file://" + sliderList.get(i).getImagePath())
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);

            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putInt("position",i);
            view_image_slider.addSlider(textSliderView);
        }

        view_image_slider.setPresetTransformer(SliderLayout.Transformer.DepthPage);
        //mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        view_image_slider.setDuration(5000);
        //view_image_slider.addOnPageChangeListener((ViewPagerEx.OnPageChangeListener) getContext());
        view_image_slider.setCustomIndicator(custom_image_indicator);
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {
       *//* int position = slider.getBundle().getInt("position");

        Intent i = new Intent(MainActivity.this, SliderDetailActivity.class);
        i.putExtra("position",position);
        i.putExtra("blockType","SliderGallery");
        i.putParcelableArrayListExtra("SliderGallery", (ArrayList<? extends Parcelable>) sliderList);
        startActivity(i);*//*
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }*/
}
