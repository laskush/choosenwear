package com.laskush.weathercloth.Weather.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.laskush.weathercloth.R;
import com.laskush.weathercloth.Utils.AppUtil;
import com.laskush.weathercloth.Utils.PrefUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class WeatherActivity extends AppCompatActivity {

    TextView toolbar_tv;
    Toolbar toolbar;

    EditText city_name;
    TextView responsetv;
    TextView fetch;

    RequestQueue requestQueue;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_weather);

        toolbar_tv = (TextView) findViewById(R.id.toolbar_tv);
        toolbar_tv.setText(getResources().getString(R.string.weather));
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);


        progressDialog = new ProgressDialog(WeatherActivity.this);
        progressDialog.setMessage("Loading.......");
        progressDialog.setTitle("Searching for Weather Info");
        requestQueue = Volley.newRequestQueue(WeatherActivity.this);

        city_name = (EditText) findViewById(R.id.city_name);
        responsetv = (TextView) findViewById(R.id.Response);
        responsetv.setVisibility(View.GONE);
        fetch = (TextView) findViewById(R.id.fetch);

        fetch.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
            @Override
            public void onClick(View view) {
                if (city_name.getText().toString().isEmpty()) {
                    showSnackbar("Enter the City Name");
                    //Toast.makeText(WeatherActivity.this, "Enter the City Name", Toast.LENGTH_LONG).show();
                } else {

                    if(AppUtil.isInternetConnectionAvailable(WeatherActivity.this)){
                        progressDialog.show();
                        HttpMethod();
                    }else{
                        showSnackbar(getString(R.string.no_internet));
                    }


                }
            }
        });


        /*String date = "2018-07-31";
        String year = date.substring(0,4);
        String month = date.substring(5,7);
        String day = date.substring(8,10);

        //Toast.makeText(this, "Year : "+ Integer.parseInt(year) + "  Month : "+ Integer.parseInt(month) + " Day : "+ Integer.parseInt(day), Toast.LENGTH_SHORT).show();


        getDayOftheWeekFromDate(Integer.parseInt(year),Integer.parseInt(month)-1
                , Integer.parseInt(day));*/


       /* String startDate = "2018-08-01";
        DateFormat df = new SimpleDateFormat("yyyy-mm-dd");
        try {
            Date date = df.parse(startDate);

            Calendar c = Calendar.getInstance();
            c.setTime(date); // yourdate is an object of type Date

            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);

            Toast.makeText(WeatherActivity.this,"Day of the week : "+dayOfWeek,Toast.LENGTH_LONG).show();
        } catch (ParseException e) {
            e.printStackTrace();
        }*/



       /* long unixdate = 1532923200;
        //long unixdate = 45;
        DateFormat formatter = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(unixdate * 1000);
        System.out.println("Formatted Date:" + formatter.format(calendar.getTime()));
        Toast.makeText(this, "CONVERTED DATE : "+  formatter.format(calendar.getTime()), Toast.LENGTH_LONG).show();*/


    }


    private void HttpMethod() {

        StringRequest sr = new StringRequest(Request.Method.GET, "http://api.worldweatheronline.com/premium/v1/weather.ashx?key=49fa51f993fc4e3fbcb22603182610&q="+city_name.getText().toString() + "&format=json&num_of_days=7",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("HttpClient", "success! response: " + response.toString());

                        if(response.toString() != null){

                            try {
                                JSONObject jsonObject = new JSONObject(response.toString());
                                if(jsonObject.has("data")){
                                    JSONObject jsonObjectData = jsonObject.getJSONObject("data");

                                    if(jsonObjectData.has("error")){

                                        JSONArray jsonArrayError = jsonObjectData.getJSONArray("error");

                                        for(int i=0; i<jsonArrayError.length();i++){
                                            JSONObject jsonObject1 = jsonArrayError.getJSONObject(0);

                                            if(jsonObject1.has("msg")){
                                                progressDialog.dismiss();
                                                String message = jsonObject1.getString("msg");
                                                showSnackbar(message);
                                                //Toast.makeText(WeatherActivity.this,message, Toast.LENGTH_SHORT).show();
                                                //responsetv.setText(message);
                                            }
                                        }
                                    }else if(jsonObjectData.has("request")){
                                        //.makeText(WeatherActivity.this, "RESPONSE : "+ response, Toast.LENGTH_SHORT).show();
                                        responsetv.setText(response);

                                        progressDialog.dismiss();
                                        PrefUtils.save_weatherValue(WeatherActivity.this,city_name.getText().toString(),true);
                                        Intent i = new Intent(WeatherActivity.this,WeatherListActvitiy.class);
                                        i.putExtra("response",response.toString());
                                        startActivity(i);
                                        finish();
                                    }


                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.e("HttpClient", "error: " + error.toString());
                        //Toast.makeText(WeatherActivity.this, "RESPONSE : NULL", Toast.LENGTH_SHORT).show();
                        showSnackbar("City not found");
                    }
                })
        {
/*            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("key","14085375aceb4090b9f113838182407");
                params.put("q","Kathmandu");
                params.put("format","json");
                params.put("num_of_days","1");
                return params;
            }*/
           /* @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }*/
        };
        requestQueue.add(sr);
    }


    private void getDayOftheWeekFromDate(int year, int month, int day) {

        Calendar calendar = new GregorianCalendar(year, month, day); // Note that Month value is 0-based. e.g., 0 for January.
        int result = calendar.get(Calendar.DAY_OF_WEEK);
        Toast.makeText(WeatherActivity.this,"Day of the week : "+result,Toast.LENGTH_LONG).show();
        switch (result) {
            case 3:
                System.out.println("It's Tuesday !");
                Toast.makeText(WeatherActivity.this,"Day of the week : "+result,Toast.LENGTH_LONG).show();
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    void showSnackbar(String message){

            Snackbar.make(findViewById(R.id.toolbar_tv), message,
                    Snackbar.LENGTH_LONG).show();
        }

}
