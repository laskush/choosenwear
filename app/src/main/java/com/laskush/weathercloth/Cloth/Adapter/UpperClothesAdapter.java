package com.laskush.weathercloth.Cloth.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.laskush.weathercloth.Cloth.Activity.SliderDetailActivity;
import com.laskush.weathercloth.Cloth.Model.ImageDTO;
import com.laskush.weathercloth.R;

import java.util.ArrayList;

public class UpperClothesAdapter extends RecyclerView.Adapter<UpperClothesAdapter.MyViewHolder> {


    private ArrayList<ImageDTO> upperClothesDTOList;
    Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imgItem;
        ProgressBar progressBar;

        public MyViewHolder(View view) {
            super(view);
            imgItem = (ImageView) view.findViewById(R.id.item_ImageView);
            progressBar = (ProgressBar) view.findViewById(R.id.item_ProgressBar);
        }
    }


    public UpperClothesAdapter(Context mContext,ArrayList<ImageDTO> upperClothesDTOList) {
        this.context = mContext;
        this.upperClothesDTOList = upperClothesDTOList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        ImageDTO imageDTO = upperClothesDTOList.get(position);

        holder.progressBar.setVisibility(View.VISIBLE);
        if (imageDTO != null) {
            holder.progressBar.setVisibility(View.GONE);
            // loadBannerImage(imageView,sliderItemDTO.getImageUrl());
            //Log.d("Banner", "images:::" + imageDTO.getIcon());

            //holder.imgItem.setImageResource(imageDTO.getIcon());
            Glide.with(context)
                    .load("file://"+upperClothesDTOList.get(position).getImagePath())
                    .into(holder.imgItem);

            holder.imgItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent i = new Intent(context, SliderDetailActivity.class);
                    i.putExtra("position",position);
                    i.putExtra("blockType","SliderGallery");
                    i.putParcelableArrayListExtra("SliderGallery", (ArrayList<? extends Parcelable>) upperClothesDTOList);
                    context.startActivity(i);
                }
            });

        } else {
            holder.progressBar.setVisibility(View.VISIBLE);
            Log.d("Banner", "Error Occur:::");
        }
    }

    @Override
    public int getItemCount() {
        return upperClothesDTOList.size();
    }

}
