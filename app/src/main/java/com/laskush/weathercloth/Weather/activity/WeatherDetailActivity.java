package com.laskush.weathercloth.Weather.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.laskush.weathercloth.MainActivity;
import com.laskush.weathercloth.R;
import com.laskush.weathercloth.Utils.PrefUtils;
import com.laskush.weathercloth.Weather.Fragment.BannerFragment;
import com.laskush.weathercloth.Weather.Model.SliderItemDTO;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class WeatherDetailActivity extends AppCompatActivity {

    //Views

    TextView toolbar_tv;
    Toolbar toolbar;

    ViewPager viewPager;

    /*For current condition */
    ImageView current_condition_imageView;
    TextView current_condition_textView;
    LinearLayout current_conditionll;


    /*For day condition*/
    TextView day_condition_textView;
    ImageView day_condition_imageView;

    TextView current_temperature_tv,high_temperature_tv,low_temperature_tv,windchill_tv,windspeed_tv,humidity_tv;
    LinearLayout current_temperaturell;

    //variables
    int currentSliderPage = 0;
    int totalSliderCount;
    List<SliderItemDTO> sliderList;
    Intent i;
    String result,day;
    int position;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_weather);

        i = getIntent();
        result = i.getStringExtra("response");
        day = i.getStringExtra("day");
        position =Integer.parseInt(i.getStringExtra("position"));

        toolbar_tv = (TextView) findViewById(R.id.toolbar_tv);
        toolbar_tv.setText(day);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);


        /* For current condition*/
        current_condition_imageView = (ImageView) findViewById(R.id.current_condition_imageView);
        current_condition_textView = (TextView) findViewById(R.id.current_condition_textView);
        current_conditionll = (LinearLayout) findViewById(R.id.current_conditionll);
        if(position == 0){

        }else{
            current_conditionll.setVisibility(View.GONE);
        }

        /*For day Condition*/
        day_condition_textView = (TextView) findViewById(R.id.day_condition_textView);
        day_condition_imageView = (ImageView) findViewById(R.id.day_condition_imageView);

        current_temperature_tv = (TextView) findViewById(R.id.current_temperature_tv);
        current_temperaturell = (LinearLayout) findViewById(R.id.current_temperaturell);
        if(position == 0){

        }else{
            current_temperaturell.setVisibility(View.GONE);
        }
        high_temperature_tv = (TextView) findViewById(R.id.high_temperature_tv);
        low_temperature_tv = (TextView) findViewById(R.id.low_temperature_tv);
        windchill_tv = (TextView) findViewById(R.id.windchill_tv);
        windspeed_tv = (TextView) findViewById(R.id.windspeed_tv);
        humidity_tv = (TextView) findViewById(R.id.humidity_tv);


        viewPager = (ViewPager) findViewById(R.id.weather_background_viewPager);

        sliderList = getSliderList();
        loadSliderContent(sliderList);

        setValueInView(result);

    }

    private void setValueInView(String result) {

        try {
            JSONObject jsonObject = new JSONObject(result);

            if(jsonObject.has("data")){
                JSONObject data = jsonObject.getJSONObject("data");

               /* if(data.has("request")){
                    JSONArray jsonArrayReqest = data.getJSONArray("request");

                    for (int i =0; i< jsonArrayReqest.length();i++){
                        JSONObject jsonObject1 = jsonArrayReqest.getJSONObject(i);

                        String type = jsonObject1.getString("type");
                        String query = jsonObject1.getString("query");
                    }

                }*/

                if(data.has("current_condition")){
                    JSONArray jsonArrayCurrentCond = data.getJSONArray("current_condition");

                    for(int i =0;i<jsonArrayCurrentCond.length();i++){

                        JSONObject jsonObject1 = jsonArrayCurrentCond.getJSONObject(i);

                        String observation_time = jsonObject1.getString("observation_time");
                        String temp_C = jsonObject1.getString("temp_C");

                        current_temperature_tv.setText(temp_C);

                        if(jsonObject1.has("weatherIconUrl")){
                            JSONArray jsonArrayweatherIconUrl = jsonObject1.getJSONArray("weatherIconUrl");
                            for(int j=0;j<jsonArrayweatherIconUrl.length();j++){

                                JSONObject jsonObject2 = jsonArrayweatherIconUrl.getJSONObject(i);
                                String weatherImageUrl = jsonObject2.getString("value");

                                Glide.with(WeatherDetailActivity.this)
                                        .load(weatherImageUrl)//.override(117,117)
                                        .into(current_condition_imageView);
                            }
                        }


                        if(jsonObject1.has("weatherDesc")){
                            JSONArray jsonArrayweatherDesc = jsonObject1.getJSONArray("weatherDesc");
                            for(int j=0;j<jsonArrayweatherDesc.length();j++){

                                JSONObject jsonObject2 = jsonArrayweatherDesc.getJSONObject(i);
                                String weatherDescription = jsonObject2.getString("value");

                                current_condition_textView.setText(weatherDescription);
                            }
                        }
                    }
                }


                if(data.has("weather")){

                    JSONArray jsonArrayweather = data.getJSONArray("weather");


                        JSONObject jsonObject1 = jsonArrayweather.getJSONObject(position);
                        String date = jsonObject1.getString("date");
                        String maxtempC = jsonObject1.getString("maxtempC");
                        String mintempC = jsonObject1.getString("mintempC");

                        high_temperature_tv.setText(maxtempC);
                        low_temperature_tv.setText(mintempC);

                        if(jsonObject1.has("hourly")){

                            JSONArray jsonArrayHourly = jsonObject1.getJSONArray("hourly");
                            JSONObject jsonObject2 = jsonArrayHourly.getJSONObject(4);


                            if(jsonObject2.has("weatherIconUrl")){
                                JSONArray jsonArrayweatherIconUrl = jsonObject2.getJSONArray("weatherIconUrl");
                                for(int j=0;j<jsonArrayweatherIconUrl.length();j++){

                                    JSONObject jsonObject3 = jsonArrayweatherIconUrl.getJSONObject(j);
                                    String weatherImageUrl = jsonObject3.getString("value");

                                    Glide.with(WeatherDetailActivity.this)
                                            .load(weatherImageUrl)//.override(117,117)
                                            .into(day_condition_imageView);
                                }
                            }


                            if(jsonObject2.has("weatherDesc")){
                                JSONArray jsonArrayweatherDesc = jsonObject2.getJSONArray("weatherDesc");
                                for(int j=0;j<jsonArrayweatherDesc.length();j++){

                                    JSONObject jsonObject3 = jsonArrayweatherDesc.getJSONObject(j);
                                    String weatherDescription = jsonObject3.getString("value");
                                    Log.d("CurrentWeatherAct : ","weatherDescription : "+weatherDescription);

                                    day_condition_textView.setText(weatherDescription);
                                }

                            }

                            String windSpeedKmph = jsonObject2.getString("windspeedKmph");
                            String humidity = jsonObject2.getString("humidity");

                            windspeed_tv.setText(windSpeedKmph);
                            humidity_tv.setText(humidity);
                        }





                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private List<SliderItemDTO> getSliderList() {
        List<SliderItemDTO> sliderList = new ArrayList<SliderItemDTO>();
        //sliderList.add(new SliderItemDTO(getResources().getString(R.string.image1), R.drawable.image1));
        sliderList.add(new SliderItemDTO(R.drawable.rainy_sunny_day));
        sliderList.add(new SliderItemDTO(R.drawable.lightning));
        sliderList.add(new SliderItemDTO(R.drawable.raining));
        sliderList.add(new SliderItemDTO(R.drawable.pertial_clousy_image));
        sliderList.add(new SliderItemDTO(R.drawable.raining));
        sliderList.add(new SliderItemDTO(R.drawable.cloudyday));


        return sliderList;

    }
    private void loadSliderContent(List<SliderItemDTO> sliderItemDTOList) {
        totalSliderCount = sliderItemDTOList.size();
        //  AppLog.showLog(TAG, "sliderItemDTOListSize::::" + sliderItemDTOList.size());
        if (sliderItemDTOList != null && sliderItemDTOList.size() > 0) {

            viewPager.setAdapter(new SlidePagerAdapter(getSupportFragmentManager(), sliderItemDTOList));
            //  circlePageIndicator.setViewPager(viewPager);
        }


        startSliderFling();
    }
    private void startSliderFling() {

        final Handler handler = new Handler();

        final Runnable Update = new Runnable() {
            public void run() {
                if (currentSliderPage == totalSliderCount) {
                    currentSliderPage = 0;
                }
                viewPager.setCurrentItem(currentSliderPage, true);
                currentSliderPage++;
            }
        };

        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {

            @Override
            public void run() {
                handler.post(Update);
            }
        }, 1000, 3000);

    }

    private class SlidePagerAdapter extends FragmentStatePagerAdapter {
        private List<SliderItemDTO> sliderList;

        public SlidePagerAdapter(FragmentManager fm, List<SliderItemDTO> mSliderList) {
            super(fm);
            this.sliderList = mSliderList;
        }

        @Override
        public Fragment getItem(int position) {

            if (sliderList != null) {
                return new BannerFragment(position, sliderList.get(position));
            } else {
                return new BannerFragment(position, null);
            }
        }

        @Override
        public int getCount() {
            return sliderList != null ? sliderList.size() : 1;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.weather_menu, menu);
        MenuItem favorite = menu.findItem(R.id.action_refresh);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            case R.id.action_refresh:
                showCityChangeDialog();

        }
        return super.onOptionsItemSelected(item);
    }

    private void showCityChangeDialog() {

        final Dialog dialog = new Dialog(WeatherDetailActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.city_change_dialogbox);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();


        final EditText city = (EditText) dialog.findViewById(R.id.city);
        ImageView close = (ImageView) dialog.findViewById(R.id.close);
        TextView save = (TextView) dialog.findViewById(R.id.save);


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(city.getText().toString().isEmpty()){
                    city.setError("Required");
                }else{
                    PrefUtils.save_weatherValue(WeatherDetailActivity.this, city.getText().toString(),true);
                    Intent i = new Intent(WeatherDetailActivity.this, MainActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    finish();
                }
                dialog.dismiss();

            }
        });

    }

}
