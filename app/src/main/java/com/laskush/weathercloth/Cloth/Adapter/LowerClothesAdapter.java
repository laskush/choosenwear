package com.laskush.weathercloth.Cloth.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.laskush.weathercloth.Cloth.Activity.SliderDetailActivity;
import com.laskush.weathercloth.Cloth.Model.ImageDTO;
import com.laskush.weathercloth.R;

import java.util.ArrayList;

public class LowerClothesAdapter extends RecyclerView.Adapter<LowerClothesAdapter.MyViewHolder> {

    private ArrayList<ImageDTO> lowerClothesDTOList;
    Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imgItem;
        ProgressBar progressBar;

        public MyViewHolder(View view) {
            super(view);
            imgItem = (ImageView) view.findViewById(R.id.item_ImageView);
            progressBar = (ProgressBar) view.findViewById(R.id.item_ProgressBar);
        }
    }


    public LowerClothesAdapter(Context mContext,ArrayList<ImageDTO> lowerClothesDTOList) {
        this.context = mContext;
        this.lowerClothesDTOList = lowerClothesDTOList;
    }

    @Override
    public LowerClothesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);

        return new LowerClothesAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        ImageDTO imageDTO = lowerClothesDTOList.get(position);

        holder.progressBar.setVisibility(View.VISIBLE);
        if (imageDTO != null) {
            holder.progressBar.setVisibility(View.GONE);
            // loadBannerImage(imageView,sliderItemDTO.getImageUrl());
           // Log.d("Banner", "images:::" + imageDTO.getIcon());
            //textViewBnner.setText(sliderItemDTO.getName());
           // holder.imgItem.setImageResource(imageDTO.getIcon());

            Glide.with(context)
                    .load("file://"+lowerClothesDTOList.get(position).getImagePath())
                    .into(holder.imgItem);

            holder.imgItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent i = new Intent(context, SliderDetailActivity.class);
                    i.putExtra("position",position);
                    i.putExtra("blockType","SliderGallery");
                    i.putParcelableArrayListExtra("SliderGallery", (ArrayList<? extends Parcelable>) lowerClothesDTOList);
                    context.startActivity(i);
                }
            });

        } else {
            holder.progressBar.setVisibility(View.VISIBLE);
            Log.d("Banner", "Error Occur:::");
        }
    }

    @Override
    public int getItemCount() {
        return lowerClothesDTOList.size();
    }
}
