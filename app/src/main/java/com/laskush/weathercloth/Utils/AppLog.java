package com.laskush.weathercloth.Utils;


import android.util.Log;

import com.laskush.weathercloth.BuildConfig;


/**
 * Created by lokex on 4/16/15.
 */
public class AppLog {

    public static void showLog(String tag, String message){
        if(BuildConfig.DEBUG) Log.d(tag, message);
    }
}
