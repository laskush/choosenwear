package com.laskush.weathercloth.Reminder1;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NavUtils;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.laskush.weathercloth.MainActivity;
import com.laskush.weathercloth.R;

public class ReminderActivity1 extends AppCompatActivity {

  private TextView mNavTitle;
  private DrawerLayout mDrawerLayout;
  private ActionBarDrawerToggle mDrawerToggle;
  private String mActivityTitle;
  private NavigationView mNavigationView;
  private View mNavHeader;
  private Toolbar mToolbar;
  private FragmentManager mFragmentManager;

  @Override
  public void onCreate(Bundle savedInstanceState) {

    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_remainder_main1);
    mToolbar = findViewById(R.id.tool_bar);
    this.setSupportActionBar(mToolbar);

    if (getSupportActionBar() != null) {
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    mFragmentManager = getSupportFragmentManager();
    /*Bundle args = new Bundle();
    args.putSerializable(ReminderParams.TYPE, ReminderType.ALL);
    Fragment reminderFragment = new ReminderFragment();
    reminderFragment.setArguments(args);
    mFragmentManager.beginTransaction().add(R.id.content_frame, reminderFragment).commit();*/

    reloadReminders(ReminderType.ALERT);

    //setupDrawer();
  }

  @Override
  public void onBackPressed() {
    Intent i = new Intent(ReminderActivity1.this,MainActivity.class);
    startActivity(i);
    finish();
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        Intent i = new Intent(ReminderActivity1.this,MainActivity.class);
        startActivity(i);
        finish();
        break;
      default:
        break;
    }
    return true;
  }

/*
  @Override
  protected void onPostCreate(Bundle savedInstanceState) {
    super.onPostCreate(savedInstanceState);
    mDrawerToggle.syncState();
  }

  @Override
  public void onConfigurationChanged(Configuration newConfig) {
    super.onConfigurationChanged(newConfig);
    mDrawerToggle.onConfigurationChanged(newConfig);
  }

  public boolean onOptionsItemSelected(MenuItem item) {
    return mDrawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
  }
*/

 /* private void setupDrawer() {
    mNavigationView = findViewById(R.id.navigation_view);
    mDrawerLayout = findViewById(R.id.drawer_layout);
    mActivityTitle = getTitle().toString();

    mNavHeader = mNavigationView.getHeaderView(0);
    mNavTitle = mNavHeader.findViewById(R.id.name);

    mNavTitle.setText(mActivityTitle);

    mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

      // This method will trigger on item Click of navigation menu
      @Override
      public boolean onNavigationItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
          case R.id.nav_home:
            mDrawerLayout.closeDrawers();
            Toast.makeText(getApplicationContext(), R.string.show_all_toast,
                Toast.LENGTH_SHORT).show();
            reloadReminders(ReminderType.ALL);
            break;
          case R.id.nav_alerts:
            mDrawerLayout.closeDrawers();
            Toast.makeText(getApplicationContext(), R.string.show_alerts_toast,
                Toast.LENGTH_SHORT).show();
            reloadReminders(ReminderType.ALERT);
            break;
          case R.id.nav_notes:
            mDrawerLayout.closeDrawers();
            Toast.makeText(getApplicationContext(), R.string.show_notes_toast,
                Toast.LENGTH_SHORT).show();
            reloadReminders(ReminderType.NOTE);
            break;
          case R.id.nav_settings:
            mDrawerLayout.closeDrawers();
            Toast.makeText(getApplicationContext(), "Coming soon!", Toast.LENGTH_SHORT).show();
            break;
          default:
            break;
        }
        return true;
      }

      ;
    });
    mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
            R.string.drawer_open, R.string.drawer_close) {

      *//** Called when a drawer has settled in a completely open state. *//*
      public void onDrawerOpened(View drawerView) {
        super.onDrawerOpened(drawerView);

      }

      *//** Called when a drawer has settled in a completely closed state. *//*
      public void onDrawerClosed(View view) {
        super.onDrawerClosed(view);
      }

    };
    mDrawerToggle.setDrawerIndicatorEnabled(true);
    mDrawerLayout.addDrawerListener(mDrawerToggle);
  }*/

  public void reloadReminders(ReminderType type) {
    Bundle args = new Bundle();
    args.putSerializable(ReminderParams.TYPE, type);
    Fragment reminderFragment = new ReminderFragment();
    reminderFragment.setArguments(args);
    mFragmentManager.beginTransaction().replace(R.id.content_frame, reminderFragment).commit();
  }

}

