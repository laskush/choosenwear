package com.laskush.weathercloth.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.laskush.weathercloth.Cloth.Model.ReminderItem;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class PrefUtils {

    public static final String SHARED_PREF_WEATHER_CLOTH = "wealther_cloth";

    public static  final String GENDER = "gender";
    public static final String CITY = "city";
    public static  final String CHECK_GENDER = "check_gender";
    public static  final String CHECK_WEATHER = "check_weather";

    public static  final String FIRST_RUN = "first_run";
    public static  final String REMAINDER_ITEM_LIST = "remainder_item_list";

    public static boolean isWeatherSave(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(SHARED_PREF_WEATHER_CLOTH, Context.MODE_PRIVATE);
        return prefs.getBoolean(CHECK_WEATHER, false);
    }

    public static boolean isGenderSave(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(SHARED_PREF_WEATHER_CLOTH, Context.MODE_PRIVATE);
        return prefs.getBoolean(CHECK_GENDER, false);
    }


    public static void save_weatherValue(Context context,String city, boolean checkweather){
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREF_WEATHER_CLOTH,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(CITY,city);
        editor.putBoolean(CHECK_WEATHER,checkweather);
        editor.commit();
    }

    public static void save_genderValue(Context context,String gender, boolean checkgender){
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREF_WEATHER_CLOTH,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(GENDER,gender);
        editor.putBoolean(CHECK_GENDER,checkgender);
        editor.commit();
    }

    public static String returnCity(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(SHARED_PREF_WEATHER_CLOTH, Context.MODE_PRIVATE);
        return prefs.getString(CITY, "kathmandu");

    }

    public static String returnGender(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(SHARED_PREF_WEATHER_CLOTH, Context.MODE_PRIVATE);
        return prefs.getString(GENDER, "MALE");

    }

    public static void save_firstRun(Context context,String first_run){
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREF_WEATHER_CLOTH,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(FIRST_RUN,first_run);
        editor.commit();
    }

    public static String returnFirstRun(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(SHARED_PREF_WEATHER_CLOTH, Context.MODE_PRIVATE);
        return prefs.getString(FIRST_RUN, "true");

    }


    public static void saveReminderList(Context context, ArrayList<ReminderItem> list, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();     // This line is IMPORTANT !!!
    }


    public static ArrayList<ReminderItem> getReminderList(Context context, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<ArrayList<ReminderItem>>() {
        }.getType();
        return gson.fromJson(json, type);
    }


}
