package com.laskush.weathercloth.Cloth.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.laskush.weathercloth.Cloth.Model.ImageDTO;
import com.laskush.weathercloth.Interface.RecylerItemClickListner;
import com.laskush.weathercloth.R;

import java.util.ArrayList;

public class SliderItemDetailAdapter extends RecyclerView.Adapter<SliderItemDetailAdapter.Holder> {

    Context mContext;
    String blockType;
    Integer pos;
    private LayoutInflater mLayoutInflater;
    private ArrayList<ImageDTO> msliderList = new ArrayList<>();

    private RecylerItemClickListner clickListener;

    public SliderItemDetailAdapter(LayoutInflater inflater, Context mContext, String blockType) {
        mLayoutInflater = inflater;
        this.mContext = mContext;
        this.blockType = blockType;
    }


    public SliderItemDetailAdapter(LayoutInflater inflater, Context mContext, String blockType, Integer pos) {
        mLayoutInflater = inflater;
        this.mContext = mContext;
        this.blockType = blockType;
        this.pos = pos;
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        if (blockType.equalsIgnoreCase("SliderGallery")) {
            view = mLayoutInflater.inflate(R.layout.list_item_slider_detail, parent, false);
        }
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        if (blockType.equalsIgnoreCase("SliderGallery")) {

            holder.bindSliderGallery(msliderList.get(position));

        }

    }

    @Override
    public int getItemCount() {

        if (blockType.equalsIgnoreCase("SliderGallery")) {
            return msliderList.size();
        }
        return 0;
    }

    public void setClickListener(RecylerItemClickListner itemClickListener) {
        this.clickListener = itemClickListener;
    }


    public void addSliderGallery(ArrayList<ImageDTO> slider) {
        msliderList.addAll(slider);
        notifyDataSetChanged();
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        //for SliderGallery
        ImageView singleimage;
        TextView txt_img_name;
        RelativeLayout overlay;
        private RecylerItemClickListner itemClickListner;


        TextView news_detail_document_name;
        ImageView document_download_iv;

        public Holder(View itemView) {
            super(itemView);
            mContext = itemView.getContext();
            itemView.setOnClickListener(this);
            if (blockType.equalsIgnoreCase("SliderGallery")) {

                singleimage = (ImageView) itemView.findViewById(R.id.slider_detail_imageView);
                singleimage.setScaleType(ImageView.ScaleType.CENTER_CROP);

            }

        }

        public void bindSliderGallery(final ImageDTO sliderDTO) {

            if (blockType.equalsIgnoreCase("SliderGallery")) {

                if (blockType.equalsIgnoreCase("SliderGallery")) {
                    Glide.with(mContext).load(sliderDTO.getImagePath()).into(singleimage);
                    //singleimage.setImageResource(sliderDTO.getIcon());

                }

            }
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }
    }
}
