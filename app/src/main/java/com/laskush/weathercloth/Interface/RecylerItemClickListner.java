package com.laskush.weathercloth.Interface;

import android.view.View;

/**
 * Created by niteshdl on 3/23/2018.
 */

public interface RecylerItemClickListner {
    void onClick(View view, int position);
}
