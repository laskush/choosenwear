package com.laskush.weathercloth.Cloth.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.laskush.weathercloth.Cloth.Adapter.SliderItemDetailAdapter;
import com.laskush.weathercloth.Cloth.Adapter.SliderItemPageAdapter;
import com.laskush.weathercloth.Cloth.Model.ImageDTO;
import com.laskush.weathercloth.Interface.RecylerItemClickListner;
import com.laskush.weathercloth.R;
import com.laskush.weathercloth.Utils.HackyViewPager;

import java.util.ArrayList;

public class SliderDetailActivity extends AppCompatActivity implements RecylerItemClickListner {

    //Constant Tags
    public final String TAG = SliderDetailActivity.class.getName();

    //Views
    private Toolbar toolbar;
    private TextView toolbar_tv;
    private HackyViewPager slider_viewPager;
    private ImageView slider_gallery_ImageView;
    private RecyclerView slider_gallery_recyclerView;

    //Instances
    SliderItemPageAdapter sliderItemPageAdapter;
    SliderItemDetailAdapter sliderItemDetailAdapter;

    //variables
    Intent i;
    ArrayList<ImageDTO> sliderList;
    int position;
    String blockType;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slider_detail);

        i = getIntent();
        position = i.getIntExtra("position",0);
        blockType = i.getStringExtra("blockType");
        sliderList = i.getParcelableArrayListExtra("SliderGallery");

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        ActionBar actionBar = getSupportActionBar();
        setTitle("");

        actionBar.setDisplayHomeAsUpEnabled(true);
        //actionBar.setDefaultDisplayHomeAsUpEnabled(true);

        toolbar_tv = (TextView) findViewById(R.id.toolbar_tv);
        toolbar_tv.setText(getString(R.string.app_name));

        slider_viewPager = (HackyViewPager) findViewById(R.id.slider_viewPager);
        slider_gallery_ImageView = (ImageView) findViewById(R.id.slider_gallery_ImageView);
        slider_gallery_recyclerView = (RecyclerView) findViewById(R.id.slider_gallery_recyclerView);

        Log.d(TAG,"sliderList : "+ sliderList.size());
        if (sliderList != null) {

            slider_viewPager.setAdapter(new SliderItemPageAdapter(SliderDetailActivity.this, sliderList, 0));
            slider_viewPager.setCurrentItem(position);

            slider_viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//                        feeditemdetail_RecyclerView.scrollToPosition(position);
                }

                @Override
                public void onPageSelected(int position) {

                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });



            if (sliderList.size() == 1) {

                slider_gallery_recyclerView.setVisibility(View.GONE);
            } else {
                try {

                    slider_gallery_recyclerView.setHasFixedSize(true);
                    slider_gallery_recyclerView.setLayoutManager(new LinearLayoutManager(SliderDetailActivity.this, LinearLayoutManager.HORIZONTAL, false));
                    //RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.HORIZONTAL,false);
                    sliderItemDetailAdapter = new SliderItemDetailAdapter(getLayoutInflater(), SliderDetailActivity.this, "SliderGallery", position);
                    sliderItemDetailAdapter.addSliderGallery(sliderList);
                    slider_gallery_recyclerView.setAdapter(sliderItemDetailAdapter);
                    sliderItemDetailAdapter.setClickListener(this);


                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        }

    }


    @Override
    public void onClick(View view, int position) {
        if (blockType.equalsIgnoreCase("SliderGallery")) {
            slider_viewPager.setCurrentItem(position);

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
