package com.laskush.weathercloth.Weather.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.laskush.weathercloth.MainActivity;
import com.laskush.weathercloth.R;
import com.laskush.weathercloth.Utils.AppUtil;
import com.laskush.weathercloth.Utils.PrefUtils;
import com.laskush.weathercloth.Weather.Model.WeatherDTO;
import com.laskush.weathercloth.Weather.adapter.WeatherListAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

public class WeatherListActvitiy extends AppCompatActivity {

    //constant
    static  String TAG = WeatherListActvitiy.class.getSimpleName();


    //View
    TextView toolbar_tv;
    Toolbar toolbar;

    ListView weather_list_view;

    //instance
    WeatherListAdapter weatherListAdapter;
    List<WeatherDTO> weatherDTOList = new ArrayList<>();
    RequestQueue requestQueue;
    ProgressDialog progressDialog;

    //variable
    String type,query ;
    String toolbar_title;
    Intent i;
    String result;

    @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weatherlist);
        requestQueue = Volley.newRequestQueue(WeatherListActvitiy.this);

        weather_list_view = (ListView) findViewById(R.id.weather_list_view);
        i = getIntent();
        result = i.getStringExtra("response");
        getCityNameFromResponse(result);

        toolbar_title = query;
        toolbar_tv = (TextView) findViewById(R.id.toolbar_tv);
        toolbar_tv.setText(query);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);


        getListFromResponse(result);

        weatherListAdapter = new WeatherListAdapter(getApplicationContext(), weatherDTOList);
        weather_list_view.setAdapter(weatherListAdapter);
        weather_list_view.setFastScrollEnabled(true);

        weather_list_view.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View item, int position, long id){

                Intent i = new Intent(WeatherListActvitiy.this,WeatherDetailActivity.class);
                i.putExtra("response",result);
                i.putExtra("position",String.valueOf(position));
                i.putExtra("day",weatherDTOList.get(position).getDay());
                startActivity(i);

               /* if(position == 0){

                }else{

                    try {
                        JSONObject jsonObject = new JSONObject(result);

                        if (jsonObject.has("data")) {
                            JSONObject data = jsonObject.getJSONObject("data");

                            if(data.has("weather")){

                                JSONArray jsonArrayweather = data.getJSONArray("weather");

                                    JSONObject jsonObject1 = jsonArrayweather.getJSONObject(position);

                                    Intent i = new Intent(WeatherListActvitiy.this,WeatherDetailActivity.class);
                                    i.putExtra("response",jsonObject1.toString());
                                    startActivity(i);
                                    }

                            }
                    }catch (JSONException e) {
                        e.printStackTrace();
                    }

                }*/
            }
        });


       /* final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Do something after 5s = 5000ms
                weatherListAdapter = new WeatherListAdapter(getApplicationContext(), weatherDTOList);
                weather_list_view.setAdapter(weatherListAdapter);
                weather_list_view.setFastScrollEnabled(true);
            }
        }, 5000);*/




    }

    private void getCityNameFromResponse(String result) {

        try {
            JSONObject jsonObject = new JSONObject(result);

            if (jsonObject.has("data")) {
                JSONObject data = jsonObject.getJSONObject("data");

                if (data.has("request")) {
                    JSONArray jsonArrayReqest = data.getJSONArray("request");

                    for (int i = 0; i < jsonArrayReqest.length(); i++) {
                        JSONObject jsonObject1 = jsonArrayReqest.getJSONObject(i);

                        type = jsonObject1.getString("type");
                        query = jsonObject1.getString("query");
                    }

                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getListFromResponse(String result) {

        weatherDTOList = new ArrayList<>();

        try {
            JSONObject jsonObject = new JSONObject(result);

            if (jsonObject.has("data")) {
                JSONObject data = jsonObject.getJSONObject("data");

                if(data.has("weather")){

                    JSONArray jsonArrayweather = data.getJSONArray("weather");

                    //Toast.makeText(this, "jsonArrayweather : "+jsonArrayweather.length(), Toast.LENGTH_SHORT).show();
                    for(int i= 0; i<jsonArrayweather.length();i++){

                        Log.d("CurrentWeatherAct : ","i = " + i);
                        //initialize DTO
                        WeatherDTO weatherDTO = new WeatherDTO();
                        JSONObject jsonObject1 = jsonArrayweather.getJSONObject(i);

                        if(jsonObject1.has("date")){

                            String date = jsonObject1.getString("date");
                            //Toast.makeText(this, "Date : "+ date, Toast.LENGTH_SHORT).show();

                            String year = date.substring(0,4);
                            String month = date.substring(5,7);
                            String day = date.substring(8,10);

                            // for setting day


                           /* if(i == 0){
                                weatherDTO.setDay("Today");
                            }else if(i == 1){
                                weatherDTO.setDay("Tomorrow");
                            }else{*/
                                String weekday = getDayOftheWeekFromDate(Integer.parseInt(year),Integer.parseInt(month)-1,Integer.parseInt(day));
                                weatherDTO.setDay(weekday);
                            /*}*/

                        }

                        if(jsonObject1.has("hourly")){

                            JSONArray jsonArrayhourly = jsonObject1.getJSONArray("hourly");
                            JSONObject jsonObject2 = jsonArrayhourly.getJSONObject(4);

                            if(jsonObject2.has("weatherIconUrl")){
                                JSONArray jsonArrayweatherIconUrl = jsonObject2.getJSONArray("weatherIconUrl");
                                for(int j=0;j<jsonArrayweatherIconUrl.length();j++){

                                    JSONObject jsonObject3 = jsonArrayweatherIconUrl.getJSONObject(j);
                                    String weatherImageUrl = jsonObject3.getString("value");
                                    weatherDTO.setImageUrl(weatherImageUrl);
                                }
                            }


                            if(jsonObject2.has("weatherDesc")){
                                JSONArray jsonArrayweatherDesc = jsonObject2.getJSONArray("weatherDesc");
                                for(int j=0;j<jsonArrayweatherDesc.length();j++){

                                    JSONObject jsonObject3 = jsonArrayweatherDesc.getJSONObject(j);
                                    String weatherDescription = jsonObject3.getString("value");
                                    Log.d("CurrentWeatherAct : ","weatherDescription : "+weatherDescription);
                                   // Toast.makeText(this, "weatherDescription : "+weatherDescription, Toast.LENGTH_SHORT).show();
                                    weatherDTO.setDesc(weatherDescription);
                                }

                            }
                        }


                        weatherDTOList.add(weatherDTO);
                    }
                }
            }
        }catch (JSONException e) {
            e.printStackTrace();
        }

        //Toast.makeText(this, "ListSize : "+weatherDTOList.size(), Toast.LENGTH_SHORT).show();

    }

    private String getDayOftheWeekFromDate(int year, int month, int day) {

        String dayOfWeek = null;
        Calendar calendar = new GregorianCalendar(year, month, day); // Note that Month value is 0-based. e.g., 0 for January.
        int result = calendar.get(Calendar.DAY_OF_WEEK);

        //Toast.makeText(CurrentWeatherActvitiy.this,"Day of the week : "+"Sunday",Toast.LENGTH_LONG).show();
        switch (result) {
            case 1:
                //Toast.makeText(CurrentWeatherActvitiy.this,"Day of the week : "+"Sunday",Toast.LENGTH_LONG).show();
                dayOfWeek = "Sunday";
                break;

            case 2:
                //Toast.makeText(CurrentWeatherActvitiy.this,"Day of the week : "+"Monday",Toast.LENGTH_LONG).show();
                dayOfWeek = "Monday";
                break;

            case 3:
                //Toast.makeText(CurrentWeatherActvitiy.this,"Day of the week : "+"Tuesday",Toast.LENGTH_LONG).show();
                dayOfWeek = "Tuesday";
                break;

            case 4:
                //Toast.makeText(CurrentWeatherActvitiy.this,"Day of the week : "+"Wednesday",Toast.LENGTH_LONG).show();
                dayOfWeek = "Wednesday";
                break;
            case 5:
                //Toast.makeText(CurrentWeatherActvitiy.this,"Day of the week : "+"Thursday",Toast.LENGTH_LONG).show();
                dayOfWeek = "Thursday";
                break;
            case 6:
                //Toast.makeText(CurrentWeatherActvitiy.this,"Day of the week : "+"Friday",Toast.LENGTH_LONG).show();
                dayOfWeek = "Friday";
                break;
            case 7:
                //Toast.makeText(CurrentWeatherActvitiy.this,"Day of the week : "+"Saturday",Toast.LENGTH_LONG).show();
                dayOfWeek = "Saturday";
                break;

            default:
                break;
        }

        return dayOfWeek;

    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.weather_menu, menu);
        MenuItem favorite = menu.findItem(R.id.action_refresh);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            case R.id.action_refresh:
                showCityChangeDialog();

        }
        return super.onOptionsItemSelected(item);
    }


    private void showCityChangeDialog() {

        final Dialog dialog = new Dialog(WeatherListActvitiy.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.city_change_dialogbox);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();


        final EditText city = (EditText) dialog.findViewById(R.id.city);
        ImageView close = (ImageView) dialog.findViewById(R.id.close);
        TextView save = (TextView) dialog.findViewById(R.id.save);


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(city.getText().toString().isEmpty()){
                    city.setError("Required");
                }else{
                    PrefUtils.save_weatherValue(WeatherListActvitiy.this, city.getText().toString(),true);

                    showSnackbar("City name changed");
                    if(AppUtil.isInternetConnectionAvailable(WeatherListActvitiy.this)){
                        progressDialog = new ProgressDialog(WeatherListActvitiy.this);
                        progressDialog.setMessage("Loading.......");
                        progressDialog.setTitle("Refreshing Weather Info");
                        progressDialog.show();
                        HttpMethod(PrefUtils.returnCity(WeatherListActvitiy.this));
                    }else{
                        showSnackbar(getString(R.string.no_internet));
                        Intent i = new Intent(WeatherListActvitiy.this, MainActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                        finish();

                    }

                }
                dialog.dismiss();

            }
        });

    }

    //fetching weather response
    private void HttpMethod(String city_name) {

        StringRequest sr = new StringRequest(Request.Method.GET, "http://api.worldweatheronline.com/premium/v1/weather.ashx?key=49fa51f993fc4e3fbcb22603182610&q="+city_name + "&format=json&num_of_days=7",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("HttpClient", "success! response: " + response.toString());

                        if(response.toString() != null){

                            try {
                                JSONObject jsonObject = new JSONObject(response.toString());
                                if(jsonObject.has("data")){
                                    JSONObject jsonObjectData = jsonObject.getJSONObject("data");

                                    if(jsonObjectData.has("error")){

                                        JSONArray jsonArrayError = jsonObjectData.getJSONArray("error");

                                        for(int i=0; i<jsonArrayError.length();i++){
                                            JSONObject jsonObject1 = jsonArrayError.getJSONObject(0);

                                            if(jsonObject1.has("msg")){
                                                progressDialog.dismiss();
                                                String message = jsonObject1.getString("msg");
                                                showSnackbar(message);
                                            }
                                        }
                                    }else if(jsonObjectData.has("request")){

                                        getCityNameFromResponse(response.toString());
                                        toolbar_tv.setText(query);
                                        getListFromResponse(response.toString());

                                        final Handler handler = new Handler();
                                        handler.postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                //Do something after 100ms
                                                progressDialog.dismiss();

                                                weatherListAdapter = new WeatherListAdapter(getApplicationContext(), weatherDTOList);
                                                weather_list_view.setAdapter(weatherListAdapter);
                                                weather_list_view.setFastScrollEnabled(true);
                                            }
                                        }, 2000);

                                    }


                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.e("HttpClient", "error: " + error.toString());
                        //Toast.makeText(WeatherActivity.this, "RESPONSE : NULL", Toast.LENGTH_SHORT).show();
                        showSnackbar("City not found");
                    }
                })
        {
/*            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("key","14085375aceb4090b9f113838182407");
                params.put("q","Kathmandu");
                params.put("format","json");
                params.put("num_of_days","1");
                return params;
            }*/
           /* @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }*/
        };
        requestQueue.add(sr);
    }

    void showSnackbar(String message){

        Snackbar.make(findViewById(R.id.toolbar_tv), message,
                Snackbar.LENGTH_LONG).show();
    }

}

