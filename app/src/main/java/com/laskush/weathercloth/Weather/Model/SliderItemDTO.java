package com.laskush.weathercloth.Weather.Model;

public class SliderItemDTO {

    int icon;

    public SliderItemDTO(int icon) {
        this.icon = icon;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }
}
