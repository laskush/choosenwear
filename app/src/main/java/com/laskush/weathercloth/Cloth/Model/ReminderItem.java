package com.laskush.weathercloth.Cloth.Model;

public class ReminderItem {

    String itemName;

    public ReminderItem(String itemName) {
        this.itemName = itemName;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }
}
