package com.laskush.weathercloth.Utils;

/**
 * Created by NITS on 5/8/15.
 */
public class AppText {
    private static final String BASE_URL="http://hostingofprologic.com/nagarpalika/";
    public static String URL_INSURANCE_PLANS=BASE_URL+"get_plans";
    public static String URL_SLIDER=BASE_URL+"api/fetchSlideshow";
    public static String URL_NEWS=BASE_URL+"api/fetchNews";
    public static String URL_SERVICES=BASE_URL+"api/fetchService";
    public static String URL_ORGANIZATION=BASE_URL+"api/fetchOrganization";
    public static String URL_PLAN=BASE_URL+"api/fetchPlan";
    public static String URL_ABOUT=BASE_URL+"api/fetchAbout";
    public static String URL_GALLERY=BASE_URL+"api/fetchGalleryCategory";
    public static String URL_MEMBER=BASE_URL+"api/fetchmember";
    public static String URL_FINANCE=BASE_URL+"get_statement";
    public static String URL_BRANCH=BASE_URL+"get_branches";
    public static String SLIDER_IMAGE_PATH=BASE_URL+"+uploads/images/slideshow/";
    public static String URL_BUDGET=BASE_URL+"api/fetchBudget";

    public static final String BOD_CATEGORY_ID="1";
    public static final String EMPLOYEE_CATEGORY_ID="2";


}
