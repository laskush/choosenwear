package com.laskush.weathercloth.Cloth.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.laskush.weathercloth.R;
import com.laskush.weathercloth.Utils.PrefUtils;

public class GenderSelectionActivty extends AppCompatActivity {

    //views
    TextView toolbar_tv;
    Toolbar toolbar;
    AppCompatCheckBox male_checkbox,female_checkbox;
    LinearLayout male_ll,female_ll,gender_save_ll;


    Intent i;
    String resultvalue = "MALE";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gender_selection);

        i = getIntent();
        resultvalue = i.getStringExtra("ClassValue");

        male_checkbox = (AppCompatCheckBox) findViewById(R.id.male_checkbox);
        female_checkbox = (AppCompatCheckBox) findViewById(R.id.female_checkbox);
        male_ll = (LinearLayout) findViewById(R.id.male_ll);
        female_ll = (LinearLayout) findViewById(R.id.female_ll);
        gender_save_ll = (LinearLayout) findViewById(R.id.gender_save_ll);

        toolbar_tv = (TextView) findViewById(R.id.toolbar_tv);
        toolbar_tv.setText("Gender");
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);



        final String savedGender = PrefUtils.returnGender(GenderSelectionActivty.this);
        final String[] newGender = new String[1];
        if(savedGender.equalsIgnoreCase("MALE")){
            male_checkbox.setChecked(true);
            female_checkbox.setChecked(false);
            newGender[0] = "MALE";
            male_ll.setBackground(getResources().getDrawable(R.drawable.selected_capsule));
            female_ll.setBackground(getResources().getDrawable(R.drawable.unselected_capsule));
        }else if(savedGender.equalsIgnoreCase("FEMALE")){
            male_checkbox.setChecked(false);
            female_checkbox.setChecked(true);
            newGender[0] = "FEMALE";
            male_ll.setBackground(getResources().getDrawable(R.drawable.unselected_capsule));
            female_ll.setBackground(getResources().getDrawable(R.drawable.selected_capsule));
        }


        male_checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                male_checkbox.setChecked(true);
                female_checkbox.setChecked(false);
                newGender[0] ="MALE";
                male_ll.setBackground(getResources().getDrawable(R.drawable.selected_capsule));
                female_ll.setBackground(getResources().getDrawable(R.drawable.unselected_capsule));
            }
        });

        female_checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                male_checkbox.setChecked(false);
                female_checkbox.setChecked(true);
                newGender[0] ="FEMALE";
                male_ll.setBackground(getResources().getDrawable(R.drawable.unselected_capsule));
                female_ll.setBackground(getResources().getDrawable(R.drawable.selected_capsule));
            }
        });

        gender_save_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PrefUtils.save_genderValue(GenderSelectionActivty.this, newGender[0], true);
                if (male_checkbox.isChecked() == false && female_checkbox.isChecked() == false) {
                    showSnackbar("Select the gender");
                }
                if (male_checkbox.isChecked() == true) {

                    if(resultvalue.equalsIgnoreCase("bestselection")){

                        PrefUtils.save_genderValue(GenderSelectionActivty.this, "MALE", true);
                        showSnackbar("Gender changed to male");
                        Intent i = new Intent(GenderSelectionActivty.this, BestMatchActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                        finish();

                    }else{

                        if(resultvalue.equalsIgnoreCase("bestselection")){

                            PrefUtils.save_genderValue(GenderSelectionActivty.this, "MALE", true);
                            showSnackbar("Gender changed to male");
                            Intent i = new Intent(GenderSelectionActivty.this, BestMatchActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            finish();

                        }else{

                            PrefUtils.save_genderValue(GenderSelectionActivty.this, "MALE", true);
                            showSnackbar("Gender changed to male");
                            Intent i = new Intent(GenderSelectionActivty.this, ClothUploadActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            finish();
                        }

                    }


                } else if (female_checkbox.isChecked() == true) {
                    /*PrefUtils.save_genderValue(GenderSelectionActivty.this, "FEMALE", true);
                    showSnackbar("Gender changed to female");
                    Intent i = new Intent(GenderSelectionActivty.this, ClothUploadActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    finish();*/


                    if(resultvalue.equalsIgnoreCase("bestselection")){

                        PrefUtils.save_genderValue(GenderSelectionActivty.this, "FEMALE", true);
                        showSnackbar("Gender changed to female");
                        Intent i = new Intent(GenderSelectionActivty.this, BestMatchActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                        finish();

                    }else{

                        if(resultvalue.equalsIgnoreCase("bestselection")){

                            PrefUtils.save_genderValue(GenderSelectionActivty.this, "FEMALE", true);
                            showSnackbar("Gender changed to female");
                            Intent i = new Intent(GenderSelectionActivty.this, BestMatchActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            finish();

                        }else{

                            PrefUtils.save_genderValue(GenderSelectionActivty.this, "FEMALE", true);
                            showSnackbar("Gender changed to female");
                            Intent i = new Intent(GenderSelectionActivty.this, ClothUploadActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            finish();
                        }

                    }
                }
            }
        });
    }



    void showSnackbar(String message){

        Snackbar.make(findViewById(R.id.toolbar_tv), message,
                Snackbar.LENGTH_LONG).show();
    }


   /* public boolean onCreateOptionsMenu(final Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.menu_gender, menu);
        MenuItem favorite = menu.findItem(R.id.gender_refresh);

        return true;
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }
}
