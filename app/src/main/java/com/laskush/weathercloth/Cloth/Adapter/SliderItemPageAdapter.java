package com.laskush.weathercloth.Cloth.Adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.laskush.weathercloth.Cloth.Model.ImageDTO;
import com.laskush.weathercloth.R;

import java.util.ArrayList;
import java.util.List;

public class SliderItemPageAdapter extends PagerAdapter {

    //conatsnt tags
    public static String TAG = SliderItemPageAdapter.class.getSimpleName();

    //variables
    private List<ImageDTO> sliderList;


    private LayoutInflater inflater;
    private Context context;
    private int type;
    private String type1 = "";

    //views
    View myImageLayout = null;

    public SliderItemPageAdapter(Context context, ArrayList<ImageDTO> sliders, int type) {
        this.context = context;
        this.sliderList = sliders;
        this.type = type;
        inflater = LayoutInflater.from(context);
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        if (type == 0) {
            return sliderList.size();
        }
        return type;
    }


    @Override
    public Object instantiateItem(ViewGroup view, int position) {

        if (type == 0) {

            myImageLayout = inflater.inflate(R.layout.pager_item_image, view, false);
            // PhotoView myImage = (PhotoView) myImageLayout.findViewById(R.id.image);
            ImageView myImage = (ImageView) myImageLayout.findViewById(R.id.image);
            TextView txt_title = (TextView) myImageLayout.findViewById(R.id.txt_title);

            final ProgressBar progressBar = (ProgressBar) myImageLayout.findViewById(R.id.activitiesProgressBar);
            String url = sliderList.get(position).getImagePath();
            myImage.setScaleType(ImageView.ScaleType.FIT_XY);


            //myImage.setImageDrawable(sliderList.get(position).getIcon());

            //.error(R.drawable.raining)
            Glide.with(context).load(url).listener(new RequestListener<String, GlideDrawable>() {
                @Override
                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                    progressBar.setVisibility(View.GONE);
                    return false;
                }

                @Override
                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                   Log.d(TAG,"Visibility: "+progressBar.getVisibility());
                    progressBar.setVisibility(View.GONE);
                    return false;
                }
            })
//                    .placeholder(R.id.activitiesProgressBar)
                    //.placeholder(R.drawable.logo)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE).into(myImage);

            view.addView(myImageLayout, 0);
        }

        return myImageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

}
