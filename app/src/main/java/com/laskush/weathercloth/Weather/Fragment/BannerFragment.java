package com.laskush.weathercloth.Weather.Fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.laskush.weathercloth.R;
import com.laskush.weathercloth.Weather.Model.SliderItemDTO;


public class BannerFragment extends Fragment {

    //constants
    private final String TAG = BannerFragment.class.getSimpleName();

    //variables
    private int position;
    private SliderItemDTO sliderItemDTO;


    //views
    ImageView imageView;

    public BannerFragment() {
        // Required empty public constructor
    }


    @SuppressLint("ValidFragment")
    public BannerFragment(int position, SliderItemDTO sliderItem) {
        this.position = position;
        this.sliderItemDTO = sliderItem;

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_banner, container, false);
        imageView = (ImageView) view.findViewById(R.id.bannerImageView);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (sliderItemDTO != null) {

            imageView.setImageResource(sliderItemDTO.getIcon());

            /*Glide.with(getActivity())
                    .load(sliderItemDTO.getImageUrl())
                    .into(imageView);*/

        } else {

        }
    }


}
