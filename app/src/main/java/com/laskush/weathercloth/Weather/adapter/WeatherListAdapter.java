package com.laskush.weathercloth.Weather.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.laskush.weathercloth.R;
import com.laskush.weathercloth.Weather.Model.WeatherDTO;

import java.util.List;

/**
 * Created by echessa on 6/18/15.
 */

public class WeatherListAdapter extends BaseAdapter {

    private Context mContext;
    private List<WeatherDTO> mWeatherList;
    public WeatherListAdapter(Context context, List<WeatherDTO> weatherList) {
        mContext = context;
        mWeatherList = weatherList;
    }

    @Override
    public int getCount() {
        return mWeatherList.size();
    }

    @Override
    public WeatherDTO getItem(int position) {
        return mWeatherList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        WeatherDTO weatherDTO = getItem(position);

        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.list_item_weather, parent, false);
            holder = new ViewHolder();
            holder.weatherImageView = (ImageView) convertView.findViewById(R.id.weather_imageView);
            holder.dayTextView = (TextView) convertView.findViewById(R.id.weather_day_tv);
            holder.desTextView = (TextView) convertView.findViewById(R.id.weather_desc_tv);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if(position == 0){
            holder.dayTextView.setText("Today");
        }else if(position == 1){
            holder.dayTextView.setText("Tomorrow");
        }else{
            holder.dayTextView.setText(weatherDTO.getDay());
        }
        //holder.dayTextView.setText(weatherDTO.getDay());
        holder.desTextView.setText(weatherDTO.getDesc());
        Glide.with(mContext)
                .load(weatherDTO.getImageUrl())//.override(117,117)
                .into(holder.weatherImageView);
        // Trigger the download of the URL asynchronously into the image view.
     //   Picasso.with(mContext).load(track.getArtworkURL()).into(holder.trackImageView);

        return convertView;
    }

    static class ViewHolder {
        ImageView weatherImageView;
        TextView dayTextView;
        TextView desTextView;
    }

}

