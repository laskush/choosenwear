package com.laskush.weathercloth.Cloth.Activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.laskush.weathercloth.R;
import com.laskush.weathercloth.Utils.PrefUtils;

public class ClothUploadActivity extends AppCompatActivity {

    //Views
    CardView summer_CardView,winter_CardView,bestmatch_CardView,bestpiece_CardView,party_CardView;
    TextView toolbar_tv;
    Toolbar toolbar;

    //variables
    public static final int MEDIA_TYPE_IMAGE = 1;
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    public static final int PERMISSIONS_MULTIPLE_REQUEST = 123;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cloth_upload);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestRuntimePermission();
        }

        toolbar_tv = (TextView) findViewById(R.id.toolbar_tv);
        toolbar_tv.setText("Cloth");
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        //initialize
        summer_CardView = (CardView) findViewById(R.id.summer_CardView);
        winter_CardView = (CardView) findViewById(R.id.winter_CardView);
        bestmatch_CardView = (CardView) findViewById(R.id.bestmatch_CardView);
        bestpiece_CardView = (CardView) findViewById(R.id.bestpiece_CardView);
        party_CardView = (CardView) findViewById(R.id.party_CardView);


        //define Clicks

        summer_CardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ClothUploadActivity.this,SummerActivity.class);
                startActivity(i);
            }
        });

        winter_CardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ClothUploadActivity.this,WinterActivity.class);
                startActivity(i);
            }
        });

        bestmatch_CardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ClothUploadActivity.this,BestMatchActivity.class);
                startActivity(i);
            }
        });

        bestpiece_CardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ClothUploadActivity.this,BestPieceActivity.class);
                startActivity(i);
            }
        });

        party_CardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent( ClothUploadActivity.this,PartyActivity.class);
                startActivity(i);
            }
        });


    }


    //For run time permission
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void requestRuntimePermission() {
        if (ContextCompat.checkSelfPermission(ClothUploadActivity.this,
                Manifest.permission.READ_EXTERNAL_STORAGE) +
                ContextCompat.checkSelfPermission(ClothUploadActivity.this,
                        Manifest.permission.CAMERA) +
                ContextCompat.checkSelfPermission(ClothUploadActivity.this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale
                    (ClothUploadActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                    ActivityCompat.shouldShowRequestPermissionRationale
                            (ClothUploadActivity.this, Manifest.permission.CAMERA) ||
                    ActivityCompat.shouldShowRequestPermissionRationale
                            (ClothUploadActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                Snackbar.make(ClothUploadActivity.this.findViewById(android.R.id.content),
                        "Please Grant Permissions to capture image",
                        Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
                        new View.OnClickListener() {
                            @RequiresApi(api = Build.VERSION_CODES.M)
                            @Override
                            public void onClick(View v) {
                                requestPermissions(
                                        new String[]{Manifest.permission
                                                .READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                        PERMISSIONS_MULTIPLE_REQUEST);
                            }
                        }).show();
            } else {
                requestPermissions(
                        new String[]{Manifest.permission
                                .READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        PERMISSIONS_MULTIPLE_REQUEST);
            }
        } else {
            // write your logic code if permission already granted
        }

    }


    public boolean onCreateOptionsMenu(final Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.menu_gender, menu);
        MenuItem favorite = menu.findItem(R.id.gender_refresh);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            case R.id.gender_refresh:
                showGenderChangeDialog();

        }
        return super.onOptionsItemSelected(item);
    }

    private void showGenderChangeDialog() {

        final Dialog dialog = new Dialog(ClothUploadActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.gender_change_dialogbox);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

        final AppCompatCheckBox male_checkbox = (AppCompatCheckBox) dialog.findViewById(R.id.male_checkbox);
        final AppCompatCheckBox  female_checkbox = (AppCompatCheckBox) dialog.findViewById(R.id.female_checkbox);
        ImageView close = (ImageView) dialog.findViewById(R.id.close);
        TextView save = (TextView) dialog.findViewById(R.id.save);

        String savedGender = PrefUtils.returnGender(ClothUploadActivity.this);

        if(savedGender.equalsIgnoreCase("MALE")){
            male_checkbox.setChecked(true);
            female_checkbox.setChecked(false);
        }else if(savedGender.equalsIgnoreCase("FEMALE")){
            male_checkbox.setChecked(false);
            female_checkbox.setChecked(true);
        }


        male_checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                male_checkbox.setChecked(true);
                female_checkbox.setChecked(false);
            }
        });

        female_checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                male_checkbox.setChecked(false);
                female_checkbox.setChecked(true);
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(male_checkbox.isChecked() == false && female_checkbox.isChecked() == false){
                    showSnackbar("Select the gender");
                }
                if(male_checkbox.isChecked() == true){
                    PrefUtils.save_genderValue(ClothUploadActivity.this, "MALE",true);
                    showSnackbar("Gender changed to male");
                    dialog.dismiss();

                }else if(female_checkbox.isChecked() == true){
                    PrefUtils.save_genderValue(ClothUploadActivity.this, "FEMALE",true);
                    showSnackbar("Gender changed to female");
                    dialog.dismiss();
                }
            }
        });

    }

    void showSnackbar(String message){

        Snackbar.make(findViewById(R.id.toolbar_tv), message,
                Snackbar.LENGTH_LONG).show();
    }

}
