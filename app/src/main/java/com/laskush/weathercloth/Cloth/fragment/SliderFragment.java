package com.laskush.weathercloth.Cloth.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.laskush.weathercloth.Cloth.Model.ImageDTO;
import com.laskush.weathercloth.R;


/**
 * Created by nitsdl on 1/11/2016.
 */
public class SliderFragment extends Fragment {

    //constants
    private final String TAG = SliderFragment.class.getSimpleName();

    //variables
    private int position;
    private ImageDTO sliderItemDTO;

    //views
    ImageView imageView;
    ProgressBar progressBarBanner;
    TextView textViewBnner;


    public SliderFragment() {
        // Required empty public constructor
    }


    @SuppressLint("ValidFragment")
    public SliderFragment(int position, ImageDTO sliderItem) {
        this.position = position;
        this.sliderItemDTO = sliderItem;

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_slider, container, false);

        //views
        imageView  = (ImageView) view.findViewById(R.id.bannerImageView);
        progressBarBanner = (ProgressBar) view.findViewById(R.id.progressBarBannerImage);
        textViewBnner = (TextView) view.findViewById(R.id.tv_banner_text);

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressBarBanner.setVisibility(View.GONE);


        if (sliderItemDTO != null) {

            Log.d(TAG, "images:::" + sliderItemDTO.getImagePath());
            textViewBnner.setText("Best Matches");
            Glide.with(getContext()).load("file://"  + sliderItemDTO.getImagePath()).into(imageView);

            //imageView.setImageResource("file://"  + sliderItemDTO.getImagePath());

        } else {

        }
    }


}
