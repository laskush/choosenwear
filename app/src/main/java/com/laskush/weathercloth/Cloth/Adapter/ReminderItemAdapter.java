package com.laskush.weathercloth.Cloth.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.laskush.weathercloth.Cloth.Model.ReminderItem;
import com.laskush.weathercloth.MainActivity;
import com.laskush.weathercloth.R;
import com.laskush.weathercloth.Remainder.RemainderActivity;
import com.laskush.weathercloth.Reminder1.ReminderActivity1;
import com.laskush.weathercloth.Utils.PrefUtils;

import java.util.ArrayList;

public class ReminderItemAdapter extends RecyclerView.Adapter<ReminderItemAdapter.MyViewHolder> {

    private ArrayList<ReminderItem> reminderItem;
    Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView reminderitem_name;
        ImageView delete_reminder_item;
        CardView reminderitem_cardView;

        public MyViewHolder(View view) {
            super(view);
            reminderitem_name = (TextView) view.findViewById(R.id.reminderitem_name);
            delete_reminder_item = (ImageView) view.findViewById(R.id.delete_reminder_item);
            reminderitem_cardView = (CardView) view.findViewById(R.id.reminderitem_cardView);
        }
    }


    public ReminderItemAdapter(Context mContext,ArrayList<ReminderItem> reminderItems) {
        this.context = mContext;
        this.reminderItem = reminderItems;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_reminder, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

            holder.reminderitem_name.setText(reminderItem.get(position).getItemName());
            holder.delete_reminder_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    showReminderDeleteDialog(position);
                    //notifyDataSetChanged();
                }
            });

            holder.reminderitem_cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent reminderIntent = new Intent(context, ReminderActivity1.class);
                    context.startActivity(reminderIntent);
                }
            });

    }

    @Override
    public int getItemCount() {
        return reminderItem.size();
    }


    private void showReminderDeleteDialog(final int position) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.delete_reminder_item_dialog);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

        TextView yes = (TextView) dialog.findViewById(R.id.yes);
        TextView no = (TextView) dialog.findViewById(R.id.no);


        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //ArrayList<ReminderItem> reminderItemList = new ArrayList<>();
                ArrayList<ReminderItem> reminderItemList1 = new ArrayList<>();
                reminderItem = PrefUtils.getReminderList(context,PrefUtils.REMAINDER_ITEM_LIST);

                for(int i =0;i<reminderItem.size();i++){
                    if(i == position){

                    }else{
                        reminderItemList1.add(new ReminderItem(reminderItem.get(i).getItemName()));
                    }
                }
                PrefUtils.saveReminderList(context,reminderItemList1,PrefUtils.REMAINDER_ITEM_LIST);
                //MainActivity.setReminderItem(reminderItemList1);
                try{
                    reminderItem.remove(position);  // remove the item from list
                    notifyItemRemoved(position);

                }catch(IndexOutOfBoundsException e){
                    Toast.makeText(context, "Cannot delete last item", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

                dialog.dismiss();

            }
        });

    }


}
