package com.laskush.weathercloth.Cloth.Activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.Nullable;

import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.laskush.weathercloth.Cloth.Adapter.LowerClothesAdapter;
import com.laskush.weathercloth.Cloth.Adapter.UpperClothesAdapter;
import com.laskush.weathercloth.Cloth.Model.ImageDTO;
import com.laskush.weathercloth.R;
import com.laskush.weathercloth.Utils.PrefUtils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class BestMatchActivity extends AppCompatActivity {

    private static final String TAG = BestMatchActivity.class.getSimpleName();

    //Views
    RecyclerView upper_recyclerView,lower_recyclerView;
    private FloatingActionButton fab,lowerfab,upperfab;
    RelativeLayout upper_no_cloths_rl,lower_no_cloths_rl;
    TextView toolbar_tv;
    Toolbar toolbar;


    //instances
    UpperClothesAdapter upperClothesAdapter;
    LowerClothesAdapter lowerClothesAdapter;

    //Variables

    ArrayList<ImageDTO> upperImageDTOArrayList = new ArrayList<>();
    ArrayList<ImageDTO> lowerImageDTOArrayList = new ArrayList<>();

    private Boolean isFabOpen = false;
    private Animation fab_open,fab_close,rotate_forward,rotate_backward;
    private Uri fileUri; // file url to store image/video
    static String setDirectoryName;
    public static final int MEDIA_TYPE_IMAGE = 1;
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    public static final int PERMISSIONS_MULTIPLE_REQUEST = 123;
    static String genderValue;
    public String folderName = "Choose n Wear";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summer);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestRuntimePermission();
        }

        toolbar_tv = (TextView) findViewById(R.id.toolbar_tv);
        toolbar_tv.setText("Best Match");
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);


        //initializing
        fab = (FloatingActionButton)findViewById(R.id.fab);
        lowerfab = (FloatingActionButton)findViewById(R.id.lowerfab);
        upperfab = (FloatingActionButton)findViewById(R.id.upperfab);
        fab_open = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_open);
        fab_close = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fab_close);
        rotate_forward = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate_forward);
        rotate_backward = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate_backward);

        lower_recyclerView = (RecyclerView) findViewById(R.id.lower_RecyclerView);
        upper_no_cloths_rl = (RelativeLayout) findViewById(R.id.upper_no_cloths_rl);
        upper_recyclerView = (RecyclerView) findViewById(R.id.upper_RecyclerView);
        lower_no_cloths_rl = (RelativeLayout) findViewById(R.id.lower_no_cloths_rl);


        //For reading gender value
        if(PrefUtils.isGenderSave(BestMatchActivity.this)){

            if(PrefUtils.returnGender(BestMatchActivity.this).equalsIgnoreCase("MALE")){
                genderValue = "MALE";
                upperfab.setImageResource(R.drawable.upper_men);
                lowerfab.setImageResource(R.drawable.lower_men);
            }else if(PrefUtils.returnGender(BestMatchActivity.this).equalsIgnoreCase("FEMALE")){
                genderValue = "FEMALE";
                upperfab.setImageResource(R.drawable.upper_women);
                lowerfab.setImageResource(R.drawable.lower_women);
            }
        }else{
            showSnackbar("Select your gender first");
        }



        getUpperClothes();
        getLowerClothes();

        //define clicks
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                animateFAB();
            }
        });


        lowerfab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                setDirectoryName = "LowerCloths";
                //method for image capturing
                // ShowDialogForCapturingImage();

                if (!isDeviceSupportCamera()) {
                                   /* Toast.makeText(getApplicationContext(),
                                            "Sorry! Your device doesn't support camera",
                                            Toast.LENGTH_LONG).show();*/
                    Snackbar.make(fab,
                            getResources().getString(R.string.CameraNotSupport),
                            Snackbar.LENGTH_LONG).show();

                    // will close the app if the device does't have camera
                    finish();
                } else {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                    fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

                    intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

                    // start the image capture Intent
                    startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
                }
            }
        });

        upperfab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                setDirectoryName = "UpperCloths";
                //method for image capturing
                //ShowDialogForCapturingImage();


                if (!isDeviceSupportCamera()) {
                                   /* Toast.makeText(getApplicationContext(),
                                            "Sorry! Your device doesn't support camera",
                                            Toast.LENGTH_LONG).show();*/
                    Snackbar.make(fab,
                            getResources().getString(R.string.CameraNotSupport),
                            Snackbar.LENGTH_LONG).show();

                    // will close the app if the device does't have camera
                    finish();
                } else {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                    fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

                    intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

                    // start the image capture Intent
                    startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
                }
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE){
            lower_no_cloths_rl.setVisibility(View.GONE);
            upper_no_cloths_rl.setVisibility(View.GONE);
            getLowerClothes();
            getUpperClothes();
        }
    }

    /**
     * Checking device has camera hardware or not
     */
    private boolean isDeviceSupportCamera() {
        if (getApplicationContext().getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA)) {
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }


    /**
     * Creating file uri to store image/video
     */
    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /**
     * returning image / video
     */
    private  File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES+"/"+ folderName +"/"+ genderValue + "/Best Match/"),
                setDirectoryName);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "Oops! Failed create "
                        + "UpperCloths" + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }


    private void getUpperClothes() {

        upperImageDTOArrayList= new ArrayList<>();
        setDirectoryName = "UpperCloths";
        File root = new File(Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES+"/"+ folderName +"/"+ genderValue + "/Best Match/"),
                setDirectoryName);
        //Log.d("Root : ", String.valueOf(root));

        // Create the storage directory if it does not exist
        if (!root.exists()) {
            upper_no_cloths_rl.setVisibility(View.VISIBLE);
            //Toast.makeText(this, "Please Upload the Upper Cloth", Toast.LENGTH_SHORT).show();
            showSnackbar("Please Upload the Upper Cloth");
        }else{

            ArrayList<File> fileList = new ArrayList<File>();
            fileList = getfile(root);

            for(int i = 0; i<fileList.size();i++){

               /* Log.d("File : ", String.valueOf(fileList.indexOf(i)));
                Toast.makeText(this, "File : "+ fileList.get(i).getAbsolutePath(), Toast.LENGTH_SHORT).show();*/
                upperImageDTOArrayList.add(new ImageDTO(" ",fileList.get(i).getAbsolutePath()));
            }

            setUpperClothes();

        }
    }



    private void getLowerClothes() {

        lowerImageDTOArrayList= new ArrayList<>();

        setDirectoryName = "LowerCloths";
        File root = new File(Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES+"/"+ folderName +"/"+ genderValue + "/Best Match/"),
                setDirectoryName);
        //Log.d("Root : ", String.valueOf(root));

        // Create the storage directory if it does not exist
        if (!root.exists()) {
            lower_no_cloths_rl.setVisibility(View.VISIBLE);
            //Toast.makeText(this, "Please Upload the Lower Coths", Toast.LENGTH_SHORT).show();
            showSnackbar("Please Upload the Lower Coths");
        }else{

            ArrayList<File> fileList = new ArrayList<File>();
            fileList = getfile(root);

            for(int i = 0; i<fileList.size();i++){
                lowerImageDTOArrayList.add(new ImageDTO(" ",fileList.get(i).getAbsolutePath()));
            }

            setLowerClothes();

        }
    }

    public ArrayList<File> getfile(File dir) {
        ArrayList<File> fileList = new ArrayList<File>();
        File listFile[] = dir.listFiles();

        // Toast.makeText(this, "listFile len : "+listFile.length, Toast.LENGTH_SHORT).show();
        if (listFile != null && listFile.length > 0) {
            for (int i = 0; i < listFile.length; i++) {

                if (listFile[i].getName().endsWith(".png")
                        || listFile[i].getName().endsWith(".jpg")
                        || listFile[i].getName().endsWith(".jpeg")
                        || listFile[i].getName().endsWith(".gif"))


                {
                    fileList.add(listFile[i]);
                }
            }
        }

        return fileList;
    }


    private void setLowerClothes() {

        lowerClothesAdapter=new LowerClothesAdapter(BestMatchActivity.this,lowerImageDTOArrayList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(BestMatchActivity.this, LinearLayout.HORIZONTAL,false);
        lower_recyclerView.setLayoutManager(mLayoutManager);
        //upper_recyclerView.setItemAnimator(new DefaultItecmAnimator());
        lower_recyclerView.setAdapter(lowerClothesAdapter);
    }

    private void setUpperClothes() {

        upperClothesAdapter=new UpperClothesAdapter(BestMatchActivity.this,upperImageDTOArrayList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(BestMatchActivity.this, LinearLayout.HORIZONTAL,false);
        upper_recyclerView.setLayoutManager(mLayoutManager);
        //upper_recyclerView.setItemAnimator(new DefaultItecmAnimator());
        upper_recyclerView.setAdapter(upperClothesAdapter);

    }


    public void animateFAB(){

        if(isFabOpen){

            //fab.startAnimation(rotate_backward);
            fab.setImageResource(R.drawable.ic_camera);
            lowerfab.startAnimation(fab_close);
            upperfab.startAnimation(fab_close);
            lowerfab.setClickable(false);
            upperfab.setClickable(false);
            isFabOpen = false;
            Log.d("Raj", "close");

        } else {

            //fab.startAnimation(rotate_forward);
            fab.setImageResource(R.drawable.ic_close);
            lowerfab.startAnimation(fab_open);
            upperfab.startAnimation(fab_open);
            lowerfab.setClickable(true);
            upperfab.setClickable(true);
            isFabOpen = true;
            Log.d("Raj","open");

        }
    }

    //For run time permission
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void requestRuntimePermission() {
        if (ContextCompat.checkSelfPermission(BestMatchActivity.this,
                Manifest.permission.READ_EXTERNAL_STORAGE) +
                ContextCompat.checkSelfPermission(BestMatchActivity.this,
                        Manifest.permission.CAMERA) +
                ContextCompat.checkSelfPermission(BestMatchActivity.this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale
                    (BestMatchActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                    ActivityCompat.shouldShowRequestPermissionRationale
                            (BestMatchActivity.this, Manifest.permission.CAMERA) ||
                    ActivityCompat.shouldShowRequestPermissionRationale
                            (BestMatchActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                Snackbar.make(BestMatchActivity.this.findViewById(android.R.id.content),
                        "Please Grant Permissions to capture image",
                        Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
                        new View.OnClickListener() {
                            @RequiresApi(api = Build.VERSION_CODES.M)
                            @Override
                            public void onClick(View v) {
                                requestPermissions(
                                        new String[]{Manifest.permission
                                                .READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                        PERMISSIONS_MULTIPLE_REQUEST);
                            }
                        }).show();
            } else {
                requestPermissions(
                        new String[]{Manifest.permission
                                .READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        PERMISSIONS_MULTIPLE_REQUEST);
            }
        } else {
            // write your logic code if permission already granted
        }

    }

    public boolean onCreateOptionsMenu(final Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.menu_gender, menu);
        MenuItem favorite = menu.findItem(R.id.gender_refresh);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            case R.id.gender_refresh:
                showGenderChangeDialog();

        }
        return super.onOptionsItemSelected(item);
    }

    private void showGenderChangeDialog() {

        final Dialog dialog = new Dialog(BestMatchActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.gender_change_dialogbox);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

        final AppCompatCheckBox male_checkbox = (AppCompatCheckBox) dialog.findViewById(R.id.male_checkbox);
        final AppCompatCheckBox  female_checkbox = (AppCompatCheckBox) dialog.findViewById(R.id.female_checkbox);
        ImageView close = (ImageView) dialog.findViewById(R.id.close);
        TextView save = (TextView) dialog.findViewById(R.id.save);

        final String savedGender = PrefUtils.returnGender(BestMatchActivity.this);
        final String[] newGender = new String[1];
        if(savedGender.equalsIgnoreCase("MALE")){
            male_checkbox.setChecked(true);
            female_checkbox.setChecked(false);
            newGender[0] = "MALE";
        }else if(savedGender.equalsIgnoreCase("FEMALE")){
            male_checkbox.setChecked(false);
            female_checkbox.setChecked(true);
            newGender[0] = "FEMALE";
        }


        male_checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                male_checkbox.setChecked(true);
                female_checkbox.setChecked(false);
                newGender[0] ="MALE";
            }
        });

        female_checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                male_checkbox.setChecked(false);
                female_checkbox.setChecked(true);
                newGender[0] ="FEMALE";
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(male_checkbox.isChecked() == false && female_checkbox.isChecked() == false){
                    showSnackbar("Select the gender");
                }
                if(newGender[0].equalsIgnoreCase(savedGender)){
                    if(newGender[0].equalsIgnoreCase("MALE")){
                        showSnackbar("Gender changed to male");
                        dialog.dismiss();
                    }else if(newGender[0].equalsIgnoreCase("FEMALE")){
                        showSnackbar("Gender changed to female");
                        dialog.dismiss();
                    }

                }else if(male_checkbox.isChecked() == true){

                    PrefUtils.save_genderValue(BestMatchActivity.this, "MALE",true);
                    showSnackbar("Gender changed to male");
                    dialog.dismiss();
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Do something after 100ms
                            Intent i = new Intent(BestMatchActivity.this,ClothUploadActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            finish();
                        }
                    }, 1000);

                }else if(female_checkbox.isChecked() == true){
                    PrefUtils.save_genderValue(BestMatchActivity.this, "FEMALE",true);
                    showSnackbar("Gender changed to female");
                    dialog.dismiss();

                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Do something after 100ms
                            Intent i = new Intent(BestMatchActivity.this,ClothUploadActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            finish();
                        }
                    }, 1000);

                }
            }
        });

    }

    void showSnackbar(String message){

        Snackbar.make(findViewById(R.id.fab), message,
                Snackbar.LENGTH_LONG).show();
    }
}
