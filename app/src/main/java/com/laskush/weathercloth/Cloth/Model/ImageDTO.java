package com.laskush.weathercloth.Cloth.Model;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;

@SuppressLint("ParcelCreator")
public class ImageDTO implements Parcelable{

    private String imageName;
    private String imagePath;
    private int icon;

    public ImageDTO(String imageName, String imagePath) {
        this.imageName = imageName;
        this.imagePath = imagePath;
    }

    public ImageDTO(String imageName, int icon) {
        this.imageName = imageName;
        this.icon = icon;
    }

    protected ImageDTO(Parcel in) {
        imageName = in.readString();
        imagePath = in.readString();
        icon = in.readInt();
    }

    public static final Creator<ImageDTO> CREATOR = new Creator<ImageDTO>() {
        @Override
        public ImageDTO createFromParcel(Parcel in) {
            return new ImageDTO(in);
        }

        @Override
        public ImageDTO[] newArray(int size) {
            return new ImageDTO[size];
        }
    };

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(imageName);
        parcel.writeString(imagePath);
        parcel.writeInt(icon);
    }
}
